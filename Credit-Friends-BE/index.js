'use strict' 
const mongoose= require('mongoose');
const app = require('./server'); 
const config = require('./config');


//Crear conexion para mongo
mongoose.connect(config.db, (err, res) => {
	if(err) { 
		return console.log("Error al conectar a la base de datos: " + err);
	} else {
        console.log('Conexion a la base de datos establecida...');
        app.listen(config.port, () =>{
            console.log("Http:\localhost:" + config.port);
        });
    }
});


var WebSocketServer = require('websocket').server;
var http = require('http');

var server = http.createServer(function(request, response){

});

//Crear Socket
server.listen(config.portWebSocket, (err, res) => {
    if(err) { 
		return console.log("Error al levantar el socket: " + err);
	} else {
        console.log("Servicio Socket activo...");
        console.log("Por el puerto Http:\\localhost:" + config.portWebSocket);
    }
});
 

var WsServer = new WebSocketServer({
    httpServer: server
});

var conexiones = [];
WsServer.on('request', function(req){
    var conection = req.accept(null, req.origin+req.resource); 
    let Conexciones = {
        NombreUsuario: req.resource.replace('/','') , 
        cnx: conection
    };
    conexiones.push(Conexciones);
    conection.on('message', function(msg){
        if(msg.type === 'utf8'){
            var obj = JSON.parse(msg.utf8Data);
            if (obj.Peticion == 'Votacion'){ 
                conexiones.filter(e => e.NombreUsuario == obj.NombreUsuario).forEach(function(f){
                    f.cnx.send(JSON.stringify(obj)); 
                });      
            }
            if (obj.Peticion == 'AltaSolicitud'){
                conexiones.filter(e => e.NombreUsuario != obj.NombreUsuario).forEach(function(f){
                    f.cnx.send(JSON.stringify(obj));
                });      
            }            
            if (obj.Peticion == 'QuitarSolicitudActual'){ 
                conexiones.filter(e => e.NombreUsuario != obj.NombreUsuario).forEach(function(f){
                    f.cnx.send(JSON.stringify(obj));
                });      
            }
            if (obj.Peticion == 'AgregarSolHistorialActual'){ 
                conexiones.filter(e => e.NombreUsuario == obj.NombreUsuario).forEach(function(f){
                    f.cnx.send(JSON.stringify(obj));
                });      
            }
        }
    }); 
    conection.on('close', function(cnx, user){
        conexiones = conexiones.filter(f => f.NombreUsuario != user);
    });
});