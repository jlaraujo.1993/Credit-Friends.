'use strict'

const express = require('express');
const usuarioCtrl = require('../controladores/usuario');
const pagoCtrl = require('../controladores/pago');
const solicitudesCtrl = require('../controladores/solicitudes'); 
const api = express.Router();


//Solicitudes
api.post('/solicitudes', solicitudesCtrl.Guardar);
api.put('/solicitudes/agregarvotacion/:solicitudid', solicitudesCtrl.AgregarVotacion);
api.get('/solicitudes/id/:solicitudid', solicitudesCtrl.ConsultarId);
api.put('/solicitudes/actualizar/:solicitudid', solicitudesCtrl.ActualizarSolicitud);
api.get('/solicitudes/pendientes/:nombreusuario',solicitudesCtrl.ConsultarPendientes);
api.get('/solicitudes/historial/:nombreusuario',solicitudesCtrl.ConsultarHistorial);
api.get('/solicitudes/actuales/:nombreusuario',solicitudesCtrl.ConsultarActuales);

//Pagos
api.post('/pagos', pagoCtrl.Guardar);
api.get('/pagos', pagoCtrl.Consultar);

//Usuarios
api.post('/usuario', usuarioCtrl.Guardar);
api.put('/usuario/cerrarsesion/:nombreusuario', usuarioCtrl.CerrarSesion);

 
module.exports = api;