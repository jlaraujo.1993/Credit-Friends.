'use strict'

const Solicitudes = require('../modelos/solicitudes');
const moment = require('moment');
 
// export enum EnumEstatus { 
//     Pendiente = 1,
//     Rechazada = 2,
//     Autorizada = 3
// } 

//Guardar Solicitud
function Guardar(req, res){
    var Fecha = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
    let solicitudes = new Solicitudes(); 
    solicitudes.Descripcion = req.body.Descripcion;
    solicitudes.Monto = req.body.Monto;
    solicitudes.Pagos = req.body.Pagos;
    solicitudes.NombreUsuario = req.body.NombreUsuario; 
    solicitudes.FechaCreacion = Fecha;
    solicitudes.Estatus = 1;

    solicitudes.save((err, solicitudesStored) => {
        if(err){
            Respuesta(res,500,'Error al guardar en la base de datos.' + err, null);
        }
        let Solicitud = { 
            _id : solicitudesStored._id,
            Descripcion : solicitudesStored.Descripcion,
            TiempoRestante :  TiempoRestante(solicitudesStored.FechaCreacion),
            Pagos: solicitudesStored.Pagos.Pagos,
            PorcentajeInteres: solicitudesStored.Pagos.PorcentajeInteres,
            Monto: solicitudesStored.Monto,
            NombreUsuario: solicitudesStored.NombreUsuario
        };
        Respuesta(res, 200,'Solicitud registrada con exito.', Solicitud);
    });
}

//Actualizar solicitud
function ActualizarSolicitud(req, res){
    Solicitudes.findById(req.params.solicitudid, (err, solicitudesStored) => {
        if(err){
            Respuesta(res,400,'Error' + err, null);
        } else if(!solicitudesStored) {
            Respuesta(res,404,'No existen la Solicitudes.', null); 
        } else { 
            var Estatus = 0;
            var Aprobadas  = solicitudesStored.ListadoUsuariosVotados.filter(ff => ff.Voto == true).length;
            var Rechazadas = solicitudesStored.ListadoUsuariosVotados.filter(ff => ff.Voto == false).length;
            if (Aprobadas >= Rechazadas)
                Estatus = 3;
            else
                Estatus = 2;
            Solicitudes.update({ _id: solicitudesStored._id}, {"$set":{Estatus: Estatus}}, function(err, doc){
				if(err){
					Respuesta(res,400,'Error' + err, null);
				} else {  
                    let publi = {
                        _id : solicitudesStored._id,
                        Descripcion : solicitudesStored.Descripcion,
                        Pagos: solicitudesStored.Pagos.Pagos,
                        PorcentajeInteres: solicitudesStored.Pagos.PorcentajeInteres,
                        Monto: solicitudesStored.Monto,
                        NombreUsuario: solicitudesStored.NombreUsuario,
                        Estatus: Estatus == 2 ? false : true 
                    }; 
					Respuesta(res,200,'Tiempo Terminado de la Solicitud : ' + solicitudesStored.Descripcion, publi);
				}
			});
        }
    });
}

//Metodo Generico para actualizar las solicitudes cuando caduco su tiempo de vida
function Actualizar(SolicitudesStored){
    var Estatus = 0;
    var Aprobadas  = SolicitudesStored.ListadoUsuariosVotados.filter(ff => ff.Voto == true).length;
    var Rechazadas = SolicitudesStored.ListadoUsuariosVotados.filter(ff => ff.Voto == false).length;
    if (Aprobadas >= Rechazadas)
        Estatus = 3;
    else
        Estatus = 2;
    Solicitudes.update({ _id: SolicitudesStored._id}, {"$set":{Estatus: Estatus}}, function(err, doc){  });
}

//Consultar Solicitudes pendientes del usuario
function ConsultarPendientes (req, res){
    Solicitudes.find({NombreUsuario:  req.params.nombreusuario, Estatus: 1}, function (err,solicitudessStored){
        if(err) {
            Respuesta(res,500,'Error al realizar la petición.' + err, null); 
        } else if(!solicitudessStored) {
            Respuesta(res,404,'No existen solicitudes pendientes.', null); 
        } else{ 
            var objPubli = []; 
            var i = 0; 
            solicitudessStored.forEach( f => {
                let publi = {
                    _id : f._id,
                    Descripcion : f.Descripcion,
                    Pagos: f.Pagos.Pagos,
                    PorcentajeInteres: f.Pagos.PorcentajeInteres,
                    Monto: f.Monto,
                    TiempoRestante :  TiempoRestante(f.FechaCreacion),
                    vtnRechazados : f.ListadoUsuariosVotados.length == 0 ? 0 : f.ListadoUsuariosVotados.filter(ff => ff.Voto == false).length,
                    vtnAprobados : f.ListadoUsuariosVotados.length == 0 ? 0 : f.ListadoUsuariosVotados.filter(ff => ff.Voto == true).length,
                    FechaCreacion: f.FechaCreacion
                }; 
                if(publi.TiempoRestante != -1){
                    objPubli.push(publi);
                } else {
                    Actualizar(f);
                }
            }); 
            Respuesta(res,200,'', objPubli.sort(sortFunction));
        }
    });
}

//Consultar las Solicitudes del usuario que ya fueron vencidas
function ConsultarHistorial (req, res){
    Solicitudes.find({NombreUsuario:  req.params.nombreusuario, Estatus: {$nin:[1]}}, function (err,solicitudesStored){
        if(err) {
            Respuesta(res,500,'Error al realizar la petición.' + err, null); 
        } else if(!solicitudesStored) {
            Respuesta(res,404,'No existen solicitudes pendientes.', null); 
        } else{ 
            var objPubli = [];
            solicitudesStored.forEach( f => {
                let publi = {
                    _id : f._id,
                    Descripcion : f.Descripcion,
                    Pagos: f.Pagos.Pagos,
                    PorcentajeInteres: f.Pagos.PorcentajeInteres,
                    Monto: f.Monto,
                    Estatus: f.Estatus == 2 ? false : true 
                };
                objPubli.push(publi);
            }); 
            Respuesta(res,200,'', objPubli.sort(sortFunction));
        }
    });
} 

//Metodos para consultar Solicitudes Actuales que no pertenescan al usuario que las creo y que esten pendientes
function ConsultarActuales (req, res){
    Solicitudes.find({NombreUsuario: {$nin:[req.params.nombreusuario]}, Estatus: 1}, function (err,solicitudessStored){
        if(err) {
            Respuesta(res,500,'Error al realizar la petición.' + err, null); 
        } else if(!solicitudessStored) {
            Respuesta(res,404,'No existen solicitudes.', null); 
        } else{ 
            var objPubli = []; 
            var i = 0; 
            solicitudessStored.forEach( f => {
                let publi = {
                    _id : f._id,
                    Descripcion : f.Descripcion,
                    TiempoRestante :  TiempoRestante(f.FechaCreacion),
                    NombreUsuario: f.NombreUsuario,
                    Pagos: f.Pagos.Pagos,
                    PorcentajeInteres: f.Pagos.PorcentajeInteres,
                    Monto: f.Monto
                }; 
                var dato = f.ListadoUsuariosVotados.length == 0 ? 0 : f.ListadoUsuariosVotados.filter(ff => ff.NombreUsuario == req.params.nombreusuario).length;
                if(dato == 0)
                    objPubli.push(publi); 
            }); 
            Respuesta(res,200,'', objPubli.sort(sortFunction));
        }
    });
} 

//Metodo para calcular el tiempo de vida de una solicitud
function TiempoRestante(FechaRegistro){
    var FechaActual = moment(new Date()).format("mm:ss"),
        FASplit = FechaActual.split(':'),
        FASegundo = Number(FASplit[0] * 60) + Number(FASplit[1]) + 12;

    FechaRegistro = moment(FechaRegistro).format("mm:ss");
    var FrSplit = FechaRegistro.split(':'),
        FRSegundo = Number(FrSplit[0] * 60) + Number(FrSplit[1]);
   
    var FFSegundo = ((Number(FRSegundo + (1 * 60))) - Number(FASegundo));
    var EsNegativo = Math.sign(FFSegundo); 
    if(EsNegativo == -1){ 
        return '-1';
    }

    var FFMMSS = (FFSegundo / 60).toFixed(2).replace('.',':');
    var mm = FFMMSS.split(':')[0];
    var ss = FFMMSS.split(':')[1];
    if(ss > 59){
        mm =  Number(mm) + Number(1); 
        ss = ss - 60;
     }
     var MM = String(mm).length == 1 ? '0'+mm : mm;
     var SS = String(ss).length == 1 ? '0'+ss : ss;
    var FF =  MM+':'+SS;

        return FF;
}

//Ordenar Listas
function sortFunction(a,b){  
    var dateA = new Date(a.Fecha).getTime();
    var dateB = new Date(b.Fecha).getTime();
    return dateA > dateB ? -1 : 1;  
};

//Metodos para agregar a la solicitud los usuarios que han votado
function AgregarVotacion(req, res){    
    Solicitudes.update({ _id: req.params.solicitudid}, 
    { 
        $addToSet: { 
            ListadoUsuariosVotados: {
                NombreUsuario: req.body.NombreUsuario,
                Voto: req.body.Voto
            }
        }       
    }, function(err, doc){
        if(err){
            Respuesta(res,400,'Error' + err, null);
        } else { 
            Solicitudes.findById(req.params.solicitudid, (errr, solicitudesStored) => {
                Respuesta(res,200,'', solicitudesStored);
            });            
        }
    });
} 

//Consultar por Id la Solicitud
function ConsultarId (req, res){
    Solicitudes.findById(req.params.solicitudid, (err, solicitudesStored) => {
        if(err){
            Respuesta(res,400,'Error' + err, null);
        } else if(!solicitudesStored) {
                Respuesta(res,404,'No existen la Solicitudes.', null); 
        } else {
            Respuesta(res,200,'', solicitudesStored); 
        }
    });
}

//Funcion para retornar la informacion
function Respuesta (res,estatus,mensaje,respuesta){
	let objRespuesta = {
		Estatus :estatus,
		Mensaje: mensaje,
		Respuesta: respuesta
	}
	return res.status(estatus).send(objRespuesta);
}

//Metodos del controlador
module.exports = {
    Guardar,
    ConsultarPendientes,
    ConsultarActuales,
    AgregarVotacion,
    ConsultarId,
    ActualizarSolicitud,
    ConsultarHistorial
}