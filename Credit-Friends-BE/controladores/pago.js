'use strict'

const Pago = require('../modelos/pago'); 

//Consul Pagos
function Consultar (req, res){ 
	Pago.find({}, (err,pago) =>{
		if(err) { 
            Respuesta(res,500,'Error al realizar la petición.' + err, null); 
        } else if(pago.length == 0) { 
           AgregarPagos(req, res);
        } else { 
            Respuesta(res, 200,'', pago);
        }
	});
}

//Agregar PAgos por default
function AgregarPagos(req, res){
    let pago = new Pago();
    pago.Pagos = 3;
    pago.PorcentajeInteres = 5; 
    pago.save((err, pagoStored) => {
        let pago2 = new Pago();
        pago2.Pagos = 6;
        pago2.PorcentajeInteres = 7; 
        pago2.save((err, pagoStored) => {
            let pago3 = new Pago();
            pago3.Pagos = 9;
            pago3.PorcentajeInteres = 12; 
            pago3.save((err, pagoStored) => {
                Consultar (req, res);
            });
        });
    });
}

//Guardar Pagos
function Guardar(req, res){
    let pago = new Pago();
    pago.Pagos = req.body.Pagos;
    pago.PorcentajeInteres = req.body.PorcentajeInteres; 
    pago.save((err, pagoStored) => {
        if(err){
            Respuesta(res,500,'Error al guardar en la base de datos.' + err, null);
        }
        Respuesta(res, 200,'', pagoStored);
    }); 
}
 
//Funcion para retornar la informacion
function Respuesta (res,estatus,mensaje,respuesta){
	let objRespuesta = {
		Estatus :estatus,
		Mensaje: mensaje,
		Respuesta: respuesta
	}
	return res.status(estatus).send(objRespuesta);
}

//Metodos del controlador
module.exports = {
	Guardar,
	Consultar
}