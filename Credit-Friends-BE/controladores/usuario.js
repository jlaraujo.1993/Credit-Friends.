'use strict'

const Usuario = require('../modelos/usuario');
const moment = require('moment');

//Cerrar Sesion
function CerrarSesion(req, res){
	Usuario.findOne({NombreUsuario: req.params.nombreusuario}, function(err, valusuarioStored){  
		Sesion(res, valusuarioStored._id, false, '');		 
	});
}

//Actualizar Usuario
function Sesion (res, Id, EstaEnSesion, Navegadorr){
	Usuario.update({ _id: Id}, {"$set":{EstaEnSesion:  EstaEnSesion, Navegador: Navegadorr}}, function(err, doc){
		if(err){
			Respuesta(res,400,'Error' + err, null);
		} else {
			Respuesta(res,200,EstaEnSesion ? 'Bienvenido' : '', null);
		}
	});
}

//Guardar y validar usuario
function Guardar(req, res){	  
	Usuario.findOne({NombreUsuario: req.body.NombreUsuario}, function(err, valusuarioStored){
		if(valusuarioStored != null){
			if(valusuarioStored.EstaEnSesion && valusuarioStored.Navegador == req.body.Navegador){ 
				Respuesta(res,409,'No permitido, el usuario ya esta en sesión en otra pestaña', null);				
			} else if(valusuarioStored.EstaEnSesion && valusuarioStored.Navegador != req.body.Navegador){ 
				Respuesta(res,409,'No permitido, el usuario ya esta en sesión en otro navegador', null);				
			} else if(!valusuarioStored.EstaEnSesion) {
				Sesion(res, valusuarioStored._id, true, req.body.Navegador);
			} 
		} else  {
			var Fecha = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
			let usuario = new Usuario();
			usuario.NombreUsuario = req.body.NombreUsuario;
			usuario.Navegador = req.body.Navegador;
			usuario.EstaEnSesion = true;
			usuario.FechaCreacion = Fecha;

			usuario.save((err, usuarioStored) => {
				if(err){
					Respuesta(res,500,'Error al guardar en la base de datos.' + err, null);
				}
				Respuesta(res, 200,'Bienvenido', usuarioStored);
			});
		}
	});
}

//Funcion para retornar la informacion
function Respuesta (res,estatus,mensaje,respuesta){
	let objRespuesta = {
		Estatus :estatus,
		Mensaje: mensaje,
		Respuesta: respuesta
	}
	return res.status(estatus).send(objRespuesta);
}

//Metodos del controlador
module.exports = {
	Guardar,
	CerrarSesion
}