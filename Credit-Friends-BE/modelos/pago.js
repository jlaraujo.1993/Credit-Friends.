const mongoose = require('mongoose');
const Schema = mongoose.Schema; 

// Entidad de Pagos
const PagoSchema = Schema({
	Pagos: String,
	PorcentajeInteres: Number
});

module.exports = mongoose.model('Pago', PagoSchema);