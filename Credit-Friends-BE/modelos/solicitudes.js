const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Entidad de Solicitudes
const SolicitudesSchema = Schema({
    Descripcion: String,
    Monto: Number,
    Pagos: {
        Pagos: Number,
        PorcentajeInteres: Number
    },
    NombreUsuario: String,
    FechaCreacion: String,
    Estatus: Number,
    ListadoUsuariosVotados:[]
});

module.exports = mongoose.model('Solicitudes', SolicitudesSchema);