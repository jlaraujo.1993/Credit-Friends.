const mongoose = require('mongoose');
const Schema = mongoose.Schema; 

// Entidad de Usuarios
const UsuarioSchema = Schema({
	NombreUsuario: String,
	Navegador: String,
	EstaEnSesion: Boolean,
	FechaCreacion: String
});

module.exports = mongoose.model('Usuario', UsuarioSchema);