define('vistas/widgets',["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Widgets = (function () {
        function Widgets() {
        }
        return Widgets;
    }());
    exports.Widgets = Widgets;
});



define('text!vistas/widgets.html', ['module'], function(module) { module.exports = "<template><require from=\"../componentes/widgets/perfil\"></require><require from=\"../componentes/widgets/historial\"></require><require from=\"../componentes/widgets/listado-solicitudes\"></require><div class=\"html ui top attached segment\"><div class=\"ui three column grid\"><div class=\"row\"><div class=\"column\"><div class=\"ui segment\"><perfil></perfil></div></div><div class=\"column\"><div class=\"ui segment\"><historial></historial></div></div><div class=\"column\"><div class=\"ui segment\"><listado-solicitudes></listado-solicitudes></div></div></div></div></div></template>"; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('vistas/login',["require", "exports", "aurelia-framework", "aurelia-event-aggregator", "eventos/eventos", "enumeradores/enum-vistas", "modelos/seguridad/sesion", "modelos/usuarios", "../enumeradores/enum-respuesta-api", "../servicios/respuesta-api", "../servicios/webapi/api-websocket"], function (require, exports, aurelia_framework_1, aurelia_event_aggregator_1, eventos, enum_vistas_1, sesion_1, usuarios_1, enum_respuesta_api_1, respuesta_api_1, api_websocket_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Login = (function () {
        function Login(ea, sesion, apiRespuesta, Usuarios, apiWebSocket) {
            this.ea = ea;
            this.sesion = sesion;
            this.apiRespuesta = apiRespuesta;
            this.Usuarios = Usuarios;
            this.apiWebSocket = apiWebSocket;
        }
        Login.prototype.Iniciar = function () {
            var _this = this;
            this.Usuarios.Guardar()
                .then(function (respuesta) {
                _this.apiRespuesta.ProcesarRespuesta(respuesta);
                if (respuesta.Estatus == enum_respuesta_api_1.EnumRespuestaAPI.Aceptado) {
                    _this.sesion.NombreUsuario = _this.Usuarios.Usuario.NombreUsuario;
                    _this.apiWebSocket.conectar();
                    _this.ea.publish(new eventos.VistaTrabajo(enum_vistas_1.EnumVistas.vistaDashboard));
                    setTimeout(function () {
                        _this.ea.publish(new eventos.AreaTrabajo(enum_vistas_1.EnumVistas.vistaWidgets));
                    }, 400);
                }
            });
        };
        Login = __decorate([
            aurelia_framework_1.autoinject,
            __metadata("design:paramtypes", [aurelia_event_aggregator_1.EventAggregator, sesion_1.Sesion, respuesta_api_1.ApiRespuesta, usuarios_1.Usuarios, api_websocket_1.ApiWebSocket])
        ], Login);
        return Login;
    }());
    exports.Login = Login;
});



define('text!vistas/login.html', ['module'], function(module) { module.exports = "<template><div class=\"ui middle aligned center aligned grid\"><div class=\"column\" style=\"margin-top:8em;max-width:60em!important\"><form class=\"ui large form\"><div class=\"ui stacked segment\"><div class=\"field\"><div class=\"ui left icon input\"><i class=\"user icon\"></i> <input type=\"text\" value.bind=\"Usuarios.Usuario.NombreUsuario\" placeholder=\"Nombre de usuario\"></div></div><button click.delegate=\"Iniciar()\" class=\"ui fluid large teal button\">Iniciar sesión</button></div></form></div></div></template>"; });




define("vistas/histrial-usuario", [],function(){});

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('vistas/historial-usuario',["require", "exports", "aurelia-framework", "aurelia-event-aggregator", "modelos/Solicitudes", "eventos/eventos", "enumeradores/enum-vistas"], function (require, exports, aurelia_framework_1, aurelia_event_aggregator_1, Solicitudes_1, eventos, enum_vistas_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var HistorialUsuario = (function () {
        function HistorialUsuario(ea, Solicitudes) {
            this.ea = ea;
            this.Solicitudes = Solicitudes;
            Solicitudes.ConsultarSolicitudesHistorialUsuario();
        }
        HistorialUsuario.prototype.Cerrar = function () {
            this.ea.publish(new eventos.AreaTrabajo(enum_vistas_1.EnumVistas.vistaWidgets));
        };
        HistorialUsuario = __decorate([
            aurelia_framework_1.autoinject,
            __metadata("design:paramtypes", [aurelia_event_aggregator_1.EventAggregator, Solicitudes_1.Solicitudes])
        ], HistorialUsuario);
        return HistorialUsuario;
    }());
    exports.HistorialUsuario = HistorialUsuario;
});



define('text!vistas/historial-usuario.html', ['module'], function(module) { module.exports = "<template><div class=\"ui middle aligned center aligned grid\"><div class=\"column\" style=\"max-width:80em!important\"><div class=\"html ui top attached segment\"><div class=\"row\"><div class=\"ui top attached label\"><h4>Historial Crediticio</h4></div><h4 class=\"ui header\"><div class=\"content\"> ${Solicitudes.NombreUsuarioHistorial} <div class=\"sub header\">Nombre Usuario</div></div></h4><table class=\"ui selectable celled table\"><thead><tr><th class=\"rigth aligned ten wide\">Solicitudes</th><th class=\"center aligned\">Estatus</th></tr></thead><tbody repeat.for=\"item of Solicitudes.SolicitudesHistorialUsuario\"><tr class=\"${item.Estatus ? 'positive' : 'negative'}\"><td><h4 class=\"ui header\"><div class=\"content\"> ${item.Descripcion} <div class=\"sub header\">${item.Pagos} pagos: ${item.PorcentajeInteres} de interés.</div><div class=\"sub header\">Monto: ${item.Monto}</div></div></h4></td><td class=\"center aligned\"><i class=\"large thumbs ${item.Estatus ? 'green up' : 'red down'} outline icon\"></i></td></tr></tbody></table></div><button class=\"ui fluid large teal button\" click.delegate=\"Cerrar()\">Cerrar</button></div></div></div></template>"; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('vistas/dashboard',["require", "exports", "aurelia-framework", "aurelia-event-aggregator", "eventos/eventos", "enumeradores/enum-vistas"], function (require, exports, aurelia_framework_1, aurelia_event_aggregator_1, eventos, enum_vistas_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Dashboard = (function () {
        function Dashboard(ea) {
            this.ea = ea;
            this.Subscribe();
        }
        Dashboard.prototype.Subscribe = function () {
            var _this = this;
            this.ea.subscribe(eventos.AreaTrabajo, function (res) {
                _this.Vista = res.vista.vista;
                _this.VistaModelo = res.vista.modelo;
            });
        };
        Dashboard.prototype.AreaTrabajo = function () {
            this.Vista = enum_vistas_1.EnumVistas.vistaWidgets["vista"];
            this.VistaModelo = enum_vistas_1.EnumVistas.vistaWidgets["modelo"];
        };
        Dashboard = __decorate([
            aurelia_framework_1.autoinject,
            __metadata("design:paramtypes", [aurelia_event_aggregator_1.EventAggregator])
        ], Dashboard);
        return Dashboard;
    }());
    exports.Dashboard = Dashboard;
});



define('text!vistas/dashboard.html', ['module'], function(module) { module.exports = "<template><require from=\"../componentes/menu-cabecero\"></require><menu-cabecero></menu-cabecero><div style=\"margin-top:5em\"><compose view.bind=\"Vista\" view-model=\"${VistaModelo}\"></compose></div></template>"; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('vistas/alta-solicitudes',["require", "exports", "aurelia-framework", "aurelia-event-aggregator", "modelos/Solicitudes", "modelos/pagos", "../enumeradores/enum-respuesta-api", "../servicios/respuesta-api", "../eventos/eventos", "../enumeradores/enum-vistas", "../servicios/webapi/api-websocket"], function (require, exports, aurelia_framework_1, aurelia_event_aggregator_1, Solicitudes_1, pagos_1, enum_respuesta_api_1, respuesta_api_1, eventos, enum_vistas_1, api_websocket_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var AltaSolicitudes = (function () {
        function AltaSolicitudes(ea, Solicitudes, Pagos, apiRespuesta, apiWebSocket) {
            this.ea = ea;
            this.Solicitudes = Solicitudes;
            this.Pagos = Pagos;
            this.apiRespuesta = apiRespuesta;
            this.apiWebSocket = apiWebSocket;
            this.Interes = '';
            this.Solicitudes.Solicitud.Descripcion = '';
            this.Solicitudes.Solicitud.Monto = null;
            Pagos.ConsultarPagos();
        }
        AltaSolicitudes.prototype.Guardar = function () {
            var _this = this;
            this.Solicitudes.Guardar()
                .then(function (respuesta) {
                _this.apiRespuesta.ProcesarRespuesta(respuesta);
                if (respuesta.Estatus == enum_respuesta_api_1.EnumRespuestaAPI.Aceptado) {
                    var wsobj = {
                        Peticion: 'AltaSolicitud',
                        SolicitudId: respuesta.Respuesta._id,
                        Descripcion: respuesta.Respuesta.Descripcion,
                        TiempoRestante: respuesta.Respuesta.TiempoRestante,
                        NombreUsuario: respuesta.Respuesta.NombreUsuario,
                        Pagos: respuesta.Respuesta.Pagos,
                        PorcentajeInteres: respuesta.Respuesta.PorcentajeInteres,
                        Monto: respuesta.Respuesta.Monto,
                    };
                    _this.apiWebSocket.EnviarMensaje(wsobj);
                    _this.ea.publish(new eventos.AreaTrabajo(enum_vistas_1.EnumVistas.vistaWidgets));
                }
            });
        };
        AltaSolicitudes.prototype.PagoIdChanged = function () {
            var _this = this;
            if (this.PagoId == 0) {
                this.Interes = '';
                this.Solicitudes.Solicitud.Pagos.Pagos = 0;
                this.Solicitudes.Solicitud.Pagos.PorcentajeInteres = 0;
            }
            else {
                var Pagos = this.Pagos.ListadoPagos.filter(function (s) { return s._id == String(_this.PagoId); });
                this.Interes = Pagos[0].PorcentajeInteres + '  % de interes.';
                this.Solicitudes.Solicitud.Pagos.Pagos = Number(Pagos[0].Pagos);
                this.Solicitudes.Solicitud.Pagos.PorcentajeInteres = Number(Pagos[0].PorcentajeInteres);
            }
        };
        __decorate([
            aurelia_framework_1.observable,
            __metadata("design:type", Number)
        ], AltaSolicitudes.prototype, "PagoId", void 0);
        AltaSolicitudes = __decorate([
            aurelia_framework_1.autoinject,
            __metadata("design:paramtypes", [aurelia_event_aggregator_1.EventAggregator, Solicitudes_1.Solicitudes, pagos_1.Pagos, respuesta_api_1.ApiRespuesta, api_websocket_1.ApiWebSocket])
        ], AltaSolicitudes);
        return AltaSolicitudes;
    }());
    exports.AltaSolicitudes = AltaSolicitudes;
});



define('text!vistas/alta-solicitudes.html', ['module'], function(module) { module.exports = "<template><div class=\"ui center aligned grid\"><div class=\"column\" style=\"margin-top:8em;max-width:60em!important\"><form class=\"ui form\"><h2 class=\"ui dividing header\">Nueva Solicitud</h2><div class=\"ui stacked segment\"><div class=\"field\"><div class=\"field\"><input type=\"text\" value.bind=\"Solicitudes.Solicitud.Descripcion\" maxlength=\"20\" placeholder=\"Descripcion\"></div></div><div class=\"field\"><div class=\"field\"><input type=\"text\" value.bind=\"Solicitudes.Solicitud.Monto\" placeholder=\"Monto\"></div></div><div class=\"fields\"><div class=\"twelve wide field\"><select class=\"ui fluid dropdown\" value.bind=\"PagoId\"><option model.bind=\"0\">Seleccione un Pago</option><option repeat.for=\"item of Pagos.ListadoPagos\" model.bind=\"item._id\">${item.Pagos} Pagos</option></select></div><div class=\"four wide field\"><label style=\"margin-top:1em\">${Interes}</label></div></div><button click.delegate=\"Guardar()\" class=\"ui fluid large teal button\">Guardar</button></div></form></div></div></template>"; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('servicios/webapi/api-websocket',["require", "exports", "aurelia-framework", "aurelia-event-aggregator", "environment", "../../modelos/seguridad/sesion", "../../eventos/eventos"], function (require, exports, aurelia_framework_1, aurelia_event_aggregator_1, environment_1, sesion_1, eventos) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ApiWebSocket = (function () {
        function ApiWebSocket(ea, sesion) {
            this.ea = ea;
            this.sesion = sesion;
        }
        ApiWebSocket.prototype.onMessage = function (ev) {
            this.ea.publish(new eventos.ServicioSocket(ev.data));
        };
        ApiWebSocket.prototype.EnviarMensaje = function (obj) {
            if (this.socket && this.socket.readyState == this.socket.OPEN) {
                this.socket.send(JSON.stringify(obj));
            }
            else if (this.socket && this.socket.readyState == this.socket.CLOSED) {
                this.conectar();
            }
        };
        ApiWebSocket.prototype.conectar = function () {
            var _this = this;
            this.socket = new WebSocket(environment_1.default.socket.servicioSokect + this.sesion.NombreUsuario);
            var self = this;
            this.socket.onmessage = function (ev) {
                _this.onMessage(ev);
            };
        };
        ApiWebSocket.prototype.cerrar = function (nombreusuario) {
            if (this.socket && this.socket.readyState == this.socket.OPEN) {
                var obj = { NombreUsuario: nombreusuario, Key: '' };
                this.socket.close(1000, nombreusuario);
            }
        };
        ApiWebSocket = __decorate([
            aurelia_framework_1.autoinject,
            __metadata("design:paramtypes", [aurelia_event_aggregator_1.EventAggregator, sesion_1.Sesion])
        ], ApiWebSocket);
        return ApiWebSocket;
    }());
    exports.ApiWebSocket = ApiWebSocket;
});



var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('servicios/webapi/api-usuarios',["require", "exports", "aurelia-framework", "environment", "../api-peticion"], function (require, exports, aurelia_framework_1, environment_1, api_peticion_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ApiUsuariosMethods = (function () {
        function ApiUsuariosMethods(apiBase) {
            this.apiBase = apiBase;
            this.apiBase = this.apiBase + "{0}";
        }
        ;
        ApiUsuariosMethods.prototype.Guardar = function () {
            return this.apiBase["format"]('');
        };
        ;
        ApiUsuariosMethods.prototype.CerrarSesion = function (nombreusuario) {
            return this.apiBase["format"]('/cerrarsesion/' + nombreusuario);
        };
        ;
        return ApiUsuariosMethods;
    }());
    var ApiUsuarios = (function () {
        function ApiUsuarios(api) {
            this.api = api;
            this.apis = new ApiUsuariosMethods(environment_1.default.apiUrl.usuarios);
        }
        ApiUsuarios.prototype.CerrarSesion = function (nombreusuario) {
            var _this = this;
            return new Promise(function (result) {
                _this.api.put(_this.apis.CerrarSesion(nombreusuario), {})
                    .then(function (respuesta) {
                    return result(respuesta);
                });
            });
        };
        ApiUsuarios.prototype.Guardar = function (Usuario) {
            var _this = this;
            return new Promise(function (result) {
                _this.api.post(_this.apis.Guardar(), Usuario)
                    .then(function (respuesta) {
                    return result(respuesta);
                });
            });
        };
        ApiUsuarios = __decorate([
            aurelia_framework_1.autoinject,
            __metadata("design:paramtypes", [api_peticion_1.ApiPeticion])
        ], ApiUsuarios);
        return ApiUsuarios;
    }());
    exports.ApiUsuarios = ApiUsuarios;
});



var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('servicios/webapi/api-solicitudes',["require", "exports", "aurelia-framework", "environment", "../api-peticion"], function (require, exports, aurelia_framework_1, environment_1, api_peticion_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ApiSolicitudesMethods = (function () {
        function ApiSolicitudesMethods(apiBase) {
            this.apiBase = apiBase;
            this.apiBase = this.apiBase + "{0}";
        }
        ;
        ApiSolicitudesMethods.prototype.Guardar = function () {
            return this.apiBase["format"]('');
        };
        ;
        ApiSolicitudesMethods.prototype.ConsultarSolicitudesPendientes = function (nombreusuario) {
            return this.apiBase["format"]('/pendientes/' + nombreusuario);
        };
        ;
        ApiSolicitudesMethods.prototype.ConsultarSolicitudesHistorial = function (nombreusuario) {
            return this.apiBase["format"]('/historial/' + nombreusuario);
        };
        ;
        ApiSolicitudesMethods.prototype.ConsultarSolicitudesActuales = function (nombreusuario) {
            return this.apiBase["format"]('/actuales/' + nombreusuario);
        };
        ;
        ApiSolicitudesMethods.prototype.AgregarVotacion = function (solicitudid) {
            return this.apiBase["format"]('/agregarvotacion/' + solicitudid);
        };
        ;
        ApiSolicitudesMethods.prototype.Actualizar = function (solicitudid) {
            return this.apiBase["format"]('/actualizar/' + solicitudid);
        };
        ;
        return ApiSolicitudesMethods;
    }());
    var ApiSolicitudes = (function () {
        function ApiSolicitudes(api) {
            this.api = api;
            this.apis = new ApiSolicitudesMethods(environment_1.default.apiUrl.solicitudes);
        }
        ApiSolicitudes.prototype.Actualizar = function (solicitudid) {
            var _this = this;
            return new Promise(function (result) {
                _this.api.put(_this.apis.Actualizar(solicitudid), {})
                    .then(function (respuesta) {
                    return result(respuesta);
                });
            });
        };
        ApiSolicitudes.prototype.AgregarVotacion = function (solicitudid, obj) {
            var _this = this;
            return new Promise(function (result) {
                _this.api.put(_this.apis.AgregarVotacion(solicitudid), obj)
                    .then(function (respuesta) {
                    return result(respuesta);
                });
            });
        };
        ApiSolicitudes.prototype.Guardar = function (Solicitud) {
            var _this = this;
            return new Promise(function (result) {
                _this.api.post(_this.apis.Guardar(), Solicitud)
                    .then(function (respuesta) {
                    return result(respuesta);
                });
            });
        };
        ApiSolicitudes.prototype.ConsultarSolicitudesPendientes = function (nombreusuario) {
            var _this = this;
            return new Promise(function (result) {
                _this.api.get(_this.apis.ConsultarSolicitudesPendientes(nombreusuario))
                    .then(function (respuesta) {
                    return result(respuesta);
                });
            });
        };
        ApiSolicitudes.prototype.ConsultarSolicitudesHistorial = function (nombreusuario) {
            var _this = this;
            return new Promise(function (result) {
                _this.api.get(_this.apis.ConsultarSolicitudesHistorial(nombreusuario))
                    .then(function (respuesta) {
                    return result(respuesta);
                });
            });
        };
        ApiSolicitudes.prototype.ConsultarSolicitudesActuales = function (nombreusuario) {
            var _this = this;
            return new Promise(function (result) {
                _this.api.get(_this.apis.ConsultarSolicitudesActuales(nombreusuario))
                    .then(function (respuesta) {
                    return result(respuesta);
                });
            });
        };
        ApiSolicitudes = __decorate([
            aurelia_framework_1.autoinject,
            __metadata("design:paramtypes", [api_peticion_1.ApiPeticion])
        ], ApiSolicitudes);
        return ApiSolicitudes;
    }());
    exports.ApiSolicitudes = ApiSolicitudes;
});



var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('servicios/webapi/api-pagos',["require", "exports", "aurelia-framework", "environment", "../api-peticion"], function (require, exports, aurelia_framework_1, environment_1, api_peticion_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ApiPagosMethods = (function () {
        function ApiPagosMethods(apiBase) {
            this.apiBase = apiBase;
            this.apiBase = this.apiBase + "{0}";
        }
        ;
        ApiPagosMethods.prototype.ConsultarPagos = function () {
            return this.apiBase["format"]('');
        };
        ;
        return ApiPagosMethods;
    }());
    var ApiPagos = (function () {
        function ApiPagos(api) {
            this.api = api;
            this.apis = new ApiPagosMethods(environment_1.default.apiUrl.pagos);
        }
        ApiPagos.prototype.ConsultarPagos = function () {
            var _this = this;
            return new Promise(function (result) {
                _this.api.get(_this.apis.ConsultarPagos())
                    .then(function (respuesta) {
                    return result(respuesta);
                });
            });
        };
        ApiPagos = __decorate([
            aurelia_framework_1.autoinject,
            __metadata("design:paramtypes", [api_peticion_1.ApiPeticion])
        ], ApiPagos);
        return ApiPagos;
    }());
    exports.ApiPagos = ApiPagos;
});



var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('servicios/respuesta-api',["require", "exports", "aurelia-framework", "../enumeradores/enum-respuesta-api", "aurelia-event-aggregator", "../eventos/eventos"], function (require, exports, aurelia_framework_1, enum_respuesta_api_1, aurelia_event_aggregator_1, eventos) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var RespuestaApi = (function () {
        function RespuestaApi() {
        }
        return RespuestaApi;
    }());
    exports.RespuestaApi = RespuestaApi;
    var ApiRespuesta = (function () {
        function ApiRespuesta(ea) {
            this.ea = ea;
        }
        ApiRespuesta.prototype.ProcesarRespuesta = function (respuesta) {
            var resultado;
            switch (respuesta.Estatus) {
                case enum_respuesta_api_1.EnumRespuestaAPI.Aceptado: {
                    if (respuesta.Mensaje != '') {
                        this.ea.publish(new eventos.MostrarMensaje('positive ', respuesta.Mensaje));
                        resultado = respuesta.Respuesta;
                    }
                    else {
                        resultado = respuesta.Respuesta;
                    }
                    break;
                }
                case enum_respuesta_api_1.EnumRespuestaAPI.NoEncontrado: {
                    if (respuesta.Mensaje != '') {
                        resultado = respuesta.Respuesta;
                    }
                    else {
                        resultado = respuesta.Respuesta;
                    }
                    break;
                }
                case enum_respuesta_api_1.EnumRespuestaAPI.ErrorInterno: {
                    if (respuesta.Mensaje != '') {
                        this.ea.publish(new eventos.MostrarMensaje('negative ', respuesta.Mensaje));
                        resultado = respuesta.Respuesta;
                    }
                    else {
                        resultado = respuesta.Respuesta;
                    }
                    break;
                }
                case enum_respuesta_api_1.EnumRespuestaAPI.ValidacionReglaNegocio: {
                    if (respuesta.Mensaje != '') {
                        this.ea.publish(new eventos.MostrarMensaje('negative ', respuesta.Mensaje));
                        resultado = respuesta.Respuesta;
                    }
                    else {
                        resultado = respuesta.Respuesta;
                    }
                    break;
                }
                case enum_respuesta_api_1.EnumRespuestaAPI.NoPermitido: {
                    if (respuesta.Mensaje != '') {
                        this.ea.publish(new eventos.MostrarMensaje('negative ', respuesta.Mensaje));
                        resultado = respuesta.Respuesta;
                    }
                    else {
                        resultado = respuesta.Respuesta;
                    }
                    break;
                }
            }
            return resultado;
        };
        ApiRespuesta = __decorate([
            aurelia_framework_1.autoinject,
            __metadata("design:paramtypes", [aurelia_event_aggregator_1.EventAggregator])
        ], ApiRespuesta);
        return ApiRespuesta;
    }());
    exports.ApiRespuesta = ApiRespuesta;
});



var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
define('servicios/api-peticion',["require", "exports", "aurelia-fetch-client", "aurelia-framework"], function (require, exports, aurelia_fetch_client_1, aurelia_framework_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ApiPeticion = (function (_super) {
        __extends(ApiPeticion, _super);
        function ApiPeticion() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        ApiPeticion.prototype.get = function (url) {
            var self = this;
            return new Promise(function (res, err) {
                self.fetch(url)
                    .then(function (respuesta) { return respuesta.json(); })
                    .then(function (respuesta) { return res(respuesta); })
                    .catch(function (error) { return err(error); });
            });
        };
        ApiPeticion.prototype.post = function (url, objeto) {
            var self = this;
            return new Promise(function (res, err) {
                var init = {};
                init.method = "post";
                init.body = aurelia_fetch_client_1.json(objeto);
                self.fetch(url, init)
                    .then(function (respuesta) { return respuesta.json(); })
                    .then(function (respuesta) { return res(respuesta); })
                    .catch(function (error) { return err(error); });
            });
        };
        ApiPeticion.prototype.put = function (url, objeto) {
            var self = this;
            return new Promise(function (res, err) {
                var init = {};
                init.method = "put";
                init.body = aurelia_fetch_client_1.json(objeto);
                self.fetch(url, init)
                    .then(function (respuesta) { return respuesta.json(); })
                    .then(function (respuesta) { return res(respuesta); })
                    .catch(function (error) { return err(error); });
            });
        };
        ApiPeticion = __decorate([
            aurelia_framework_1.autoinject
        ], ApiPeticion);
        return ApiPeticion;
    }(aurelia_fetch_client_1.HttpClient));
    exports.ApiPeticion = ApiPeticion;
});



define('resources/index',["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    function configure(config) {
    }
    exports.configure = configure;
});



var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('modelos/usuarios',["require", "exports", "aurelia-framework", "servicios/webapi/api-usuarios", "dto/dtousuarios", "enumeradores/enum-respuesta-api", "servicios/respuesta-api"], function (require, exports, aurelia_framework_1, api_usuarios_1, dtousuarios_1, enum_respuesta_api_1, respuesta_api_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Usuarios = (function () {
        function Usuarios(peticion) {
            this.peticion = peticion;
            this.Usuario = new dtousuarios_1.DtoUsuarios();
            this.Respuesta = new respuesta_api_1.RespuestaApi();
        }
        Usuarios.prototype.CerrarSesion = function (NombreUsuario) {
            var _this = this;
            return new Promise(function (result) {
                _this.peticion.CerrarSesion(NombreUsuario)
                    .then(function (res) {
                    return result(res);
                });
            });
        };
        Usuarios.prototype.Guardar = function () {
            var _this = this;
            if (this.Usuario.NombreUsuario.trim() == "") {
                this.Respuesta.Estatus = enum_respuesta_api_1.EnumRespuestaAPI.ValidacionReglaNegocio;
                this.Respuesta.Mensaje = "El nombre de usuario es campo obligatorio.";
                this.Respuesta.Respuesta = null;
                return new Promise(function (result) {
                    return result(_this.Respuesta);
                });
            }
            else {
                return new Promise(function (result) {
                    _this.peticion.Guardar(_this.Usuario)
                        .then(function (res) {
                        return result(res);
                    });
                });
            }
        };
        Usuarios = __decorate([
            aurelia_framework_1.autoinject,
            __metadata("design:paramtypes", [api_usuarios_1.ApiUsuarios])
        ], Usuarios);
        return Usuarios;
    }());
    exports.Usuarios = Usuarios;
});



define('modelos/seguridad/sesion',["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Sesion = (function () {
        function Sesion() {
            this.NombreUsuario = '';
        }
        return Sesion;
    }());
    exports.Sesion = Sesion;
});



var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('modelos/pagos',["require", "exports", "aurelia-framework", "aurelia-event-aggregator", "servicios/webapi/api-pagos"], function (require, exports, aurelia_framework_1, aurelia_event_aggregator_1, api_pagos_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Pagos = (function () {
        function Pagos(ea, peticion) {
            this.ea = ea;
            this.peticion = peticion;
            this.ListadoPagos = [];
        }
        Pagos.prototype.ConsultarPagos = function () {
            var _this = this;
            this.peticion.ConsultarPagos()
                .then(function (res) {
                _this.ListadoPagos = res.Respuesta;
            });
        };
        Pagos = __decorate([
            aurelia_framework_1.autoinject,
            __metadata("design:paramtypes", [aurelia_event_aggregator_1.EventAggregator, api_pagos_1.ApiPagos])
        ], Pagos);
        return Pagos;
    }());
    exports.Pagos = Pagos;
});



var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('modelos/Solicitudes',["require", "exports", "aurelia-framework", "aurelia-event-aggregator", "../eventos/eventos", "dto/dtoSolicitudes", "servicios/webapi/api-solicitudes", "enumeradores/enum-respuesta-api", "servicios/respuesta-api", "../modelos/seguridad/sesion", "../servicios/webapi/api-websocket", "../servicios/respuesta-api", "../modelos/usuarios"], function (require, exports, aurelia_framework_1, aurelia_event_aggregator_1, eventos, DtoSolicitudes, api_solicitudes_1, enum_respuesta_api_1, respuesta_api_1, sesion_1, api_websocket_1, respuesta_api_2, usuarios_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Solicitudes = (function () {
        function Solicitudes(ea, Usuario, peticion, sesion, apiRespuesta, apiWebSocket) {
            this.ea = ea;
            this.Usuario = Usuario;
            this.peticion = peticion;
            this.sesion = sesion;
            this.apiRespuesta = apiRespuesta;
            this.apiWebSocket = apiWebSocket;
            this.SolicitudesPendientes = [];
            this.SolicitudesHistorial = [];
            this.SolicitudesHistorialUsuario = [];
            this.SolicitudesActuales = [];
            this.Solicitud = new DtoSolicitudes.DtoSolicitud();
            this.Respuesta = new respuesta_api_1.RespuestaApi();
            this.NombreUsuarioHistorial = "";
            this.EjecutarTiempo();
            this.SubsCribe();
        }
        Solicitudes.prototype.SubsCribe = function () {
            var _this = this;
            this.ea.subscribe(eventos.ServicioSocket, function (res) {
                var obj = JSON.parse(res._id);
                if (obj.Peticion == "Votacion") {
                    _this.ActualizarPendientes(obj);
                }
                if (obj.Peticion == "AltaSolicitud") {
                    _this.AgregarSolicitudActual(obj);
                }
                if (obj.Peticion == "QuitarSolicitudActual") {
                    _this.QuitarSolicitudActual(obj);
                }
                if (obj.Peticion == "AgregarSolHistorialActual") {
                    _this.AgregarSolicitudHistorial(obj);
                }
            });
        };
        Solicitudes.prototype.ActualizarPendientes = function (obj) {
            this.SolicitudesPendientes = this.SolicitudesPendientes.map(function (m) {
                if (m._id == obj.SolicitudId) {
                    if (obj.Voto)
                        m.vtnAprobados = m.vtnAprobados + 1;
                    else
                        m.vtnRechazados = m.vtnRechazados + 1;
                }
                return m;
            });
        };
        Solicitudes.prototype.AgregarSolicitudActual = function (obj) {
            var _this = this;
            var SolActual = new DtoSolicitudes.DtoSolicitudesActuales();
            SolActual._id = obj.SolicitudId;
            SolActual.Descripcion = obj.Descripcion;
            SolActual.TiempoRestante = obj.TiempoRestante;
            SolActual.NombreUsuario = obj.NombreUsuario;
            SolActual.Monto = obj.Monto;
            SolActual.Pagos = obj.Pagos;
            SolActual.PorcentajeInteres = obj.PorcentajeInteres;
            setTimeout(function () {
                _this.SolicitudesActuales.unshift(SolActual);
            }, 1000);
        };
        Solicitudes.prototype.QuitarSolicitudActual = function (obj) {
            this.SolicitudesActuales = this.SolicitudesActuales.filter(function (f) { return f._id != obj.SolicitudId; });
        };
        Solicitudes.prototype.AgregarSolicitudHistorial = function (obj) {
            this.SolicitudesHistorial.unshift(obj);
            this.SolicitudesPendientes = this.SolicitudesPendientes.map(function (s) {
                if (s._id == obj._id)
                    s.Visto = 'none';
                return s;
            });
        };
        Solicitudes.prototype.EjecutarTiempo = function () {
            var _this = this;
            if (this.SolicitudesPendientes.length != 0) {
                this.SolicitudesPendientes = this.SolicitudesPendientes.map(function (s) {
                    s.TiempoRestante = _this.CalcularTiempo(s.TiempoRestante);
                    if (s.TiempoRestante != '00:00') {
                        if (s.Visto != 'none')
                            s.Visto = '';
                    }
                    else {
                        if (s.Visto != 'none') {
                            s.Visto = 'none';
                            _this.ActualizarSolicitud(s._id);
                        }
                    }
                    return s;
                });
            }
            this.SolicitudesActuales = this.SolicitudesActuales.map(function (s) {
                s.TiempoRestante = _this.CalcularTiempo(s.TiempoRestante);
                if (s.TiempoRestante != '00:00') {
                    if (s.Visto != 'none')
                        s.Visto = '';
                }
                else {
                    s.Visto = 'none';
                    _this.ActualizarSolicitud(s._id);
                }
                return s;
            });
            setTimeout(function () {
                _this.EjecutarTiempo();
            }, 1000);
        };
        Solicitudes.prototype.ActualizarSolicitud = function (Id) {
            var _this = this;
            this.peticion.Actualizar(Id)
                .then(function (result) {
                _this.apiRespuesta.ProcesarRespuesta(result);
                if (result.Estatus == enum_respuesta_api_1.EnumRespuestaAPI.Aceptado) {
                    var wsobj = {
                        Peticion: 'QuitarSolicitudActual',
                        SolicitudId: result.Respuesta._id,
                        NombreUsuario: result.Respuesta.NombreUsuario
                    };
                    _this.apiWebSocket.EnviarMensaje(wsobj);
                    var wsobjl = {
                        Peticion: 'AgregarSolHistorialActual',
                        _id: result.Respuesta._id,
                        Descripcion: result.Respuesta.Descripcion,
                        Pagos: result.Respuesta.Pagos,
                        PorcentajeInteres: result.Respuesta.PorcentajeInteres,
                        Monto: result.Respuesta.Monto,
                        Estatus: result.Respuesta.Estatus,
                        NombreUsuario: result.Respuesta.NombreUsuario
                    };
                    _this.apiWebSocket.EnviarMensaje(wsobjl);
                }
            });
        };
        Solicitudes.prototype.ConsultarSolicitudesPendientes = function () {
            var _this = this;
            this.peticion.ConsultarSolicitudesPendientes(this.sesion.NombreUsuario)
                .then(function (res) {
                _this.SolicitudesPendientes = res.Respuesta;
            });
        };
        Solicitudes.prototype.ConsultarSolicitudesHistorial = function () {
            var _this = this;
            this.peticion.ConsultarSolicitudesHistorial(this.sesion.NombreUsuario)
                .then(function (res) {
                _this.SolicitudesHistorial = res.Respuesta;
            });
        };
        Solicitudes.prototype.ConsultarSolicitudesHistorialUsuario = function () {
            var _this = this;
            console.log(this.NombreUsuarioHistorial);
            this.peticion.ConsultarSolicitudesHistorial(this.NombreUsuarioHistorial)
                .then(function (res) {
                console.log(res);
                _this.SolicitudesHistorialUsuario = res.Respuesta;
            });
        };
        Solicitudes.prototype.ConsultarSolicitudesActuales = function () {
            var _this = this;
            this.peticion.ConsultarSolicitudesActuales(this.sesion.NombreUsuario)
                .then(function (res) {
                _this.SolicitudesActuales = res.Respuesta;
            });
        };
        Solicitudes.prototype.VotacionRecApr = function (item, tipo) {
            var obj = {
                NombreUsuario: this.sesion.NombreUsuario,
                Voto: tipo
            };
            this.peticion.AgregarVotacion(item._id, obj);
            this.SolicitudesActuales = this.SolicitudesActuales.filter(function (f) { return f._id != item._id; });
            var wsobj = {
                Peticion: 'Votacion',
                SolicitudId: item._id,
                NombreUsuario: item.NombreUsuario,
                Voto: tipo
            };
            this.apiWebSocket.EnviarMensaje(wsobj);
        };
        Solicitudes.prototype.esEntero = function (numero) {
            if (isNaN(numero)) {
                return false;
            }
            else {
                if (numero % 1 == 0) {
                    return true;
                }
                else {
                    return true;
                }
            }
        };
        Solicitudes.prototype.Guardar = function () {
            var _this = this;
            this.Respuesta.Estatus = enum_respuesta_api_1.EnumRespuestaAPI.ValidacionReglaNegocio;
            this.Respuesta.Respuesta = null;
            if (this.Solicitud.Descripcion.trim() == "") {
                this.Respuesta.Mensaje = "Descripción es campo obligatorio.";
                return new Promise(function (result) {
                    return result(_this.Respuesta);
                });
            }
            else if (this.Solicitud.Monto == undefined) {
                this.Respuesta.Mensaje = "El monto es campo obligatorio.";
                return new Promise(function (result) {
                    return result(_this.Respuesta);
                });
            }
            else if (!this.esEntero(this.Solicitud.Monto)) {
                this.Respuesta.Mensaje = "El monto debe ser numérico.";
                return new Promise(function (result) {
                    return result(_this.Respuesta);
                });
            }
            else if (this.Solicitud.Pagos.Pagos == 0) {
                this.Respuesta.Mensaje = "El pago es campo obligatorio.";
                return new Promise(function (result) {
                    return result(_this.Respuesta);
                });
            }
            else {
                return new Promise(function (result) {
                    _this.Solicitud.NombreUsuario = _this.sesion.NombreUsuario;
                    _this.peticion.Guardar(_this.Solicitud)
                        .then(function (res) {
                        return result(res);
                    });
                });
            }
        };
        Solicitudes.prototype.CalcularTiempo = function (minutos) {
            var spl = minutos.split(":");
            var min = spl[0];
            var seg = String(Number(spl[1]) - 1);
            if (seg == '-1') {
                seg = '59';
                min = '0' + Number(min - 1);
            }
            if (min.length == 1)
                min = '0' + min;
            if (seg.length == 1)
                seg = '0' + seg;
            minutos = min + ":" + seg;
            return minutos;
        };
        Solicitudes = __decorate([
            aurelia_framework_1.autoinject,
            __metadata("design:paramtypes", [aurelia_event_aggregator_1.EventAggregator, usuarios_1.Usuarios, api_solicitudes_1.ApiSolicitudes, sesion_1.Sesion, respuesta_api_2.ApiRespuesta, api_websocket_1.ApiWebSocket])
        ], Solicitudes);
        return Solicitudes;
    }());
    exports.Solicitudes = Solicitudes;
});



define('main',["require", "exports", "environment", "modelos/seguridad/sesion"], function (require, exports, environment_1, sesion_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    function configure(aurelia) {
        aurelia.use
            .standardConfiguration()
            .feature('resources')
            .plugin("aurelia-validation")
            .plugin("moment");
        aurelia.use
            .singleton(sesion_1.Sesion);
        if (environment_1.default.debug) {
            aurelia.use.developmentLogging();
        }
        if (environment_1.default.testing) {
            aurelia.use.plugin('aurelia-testing');
        }
        aurelia.start().then(function () { return aurelia.setRoot(); });
    }
    exports.configure = configure;
});



define('eventos/eventos',["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var AreaTrabajo = (function () {
        function AreaTrabajo(vista) {
            this.vista = vista;
        }
        return AreaTrabajo;
    }());
    exports.AreaTrabajo = AreaTrabajo;
    var VistaTrabajo = (function () {
        function VistaTrabajo(vista) {
            this.vista = vista;
        }
        return VistaTrabajo;
    }());
    exports.VistaTrabajo = VistaTrabajo;
    var MostrarMensaje = (function () {
        function MostrarMensaje(claseAlerta, mensaje) {
            this.claseAlerta = claseAlerta;
            this.mensaje = mensaje;
        }
        return MostrarMensaje;
    }());
    exports.MostrarMensaje = MostrarMensaje;
    var ServicioSocket = (function () {
        function ServicioSocket(_id) {
            this._id = _id;
        }
        return ServicioSocket;
    }());
    exports.ServicioSocket = ServicioSocket;
});



define('environment',["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.default = {
        debug: true,
        testing: true,
        apiUrl: {
            pruebas: 'http://www.mocky.io/v2/5b903ee02e0000ca2aa89e76',
            pruebasPagos: 'http://www.mocky.io/v2/5b9062252e00000c2ba89eb2',
            solicitudes: 'http://localhost:9001/api/solicitudes',
            usuarios: 'http://localhost:9001/api/usuario',
            pagos: 'http://localhost:9001/api/pagos',
        },
        socket: {
            servicioSokect: 'ws://localhost:1337/'
        }
    };
});



define('enumeradores/enum-vistas',["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var EnumVistas;
    (function (EnumVistas) {
        EnumVistas[EnumVistas["vistaNinguna"] = { vista: '', modelo: '' }] = "vistaNinguna";
        EnumVistas[EnumVistas["vistaLogin"] = { vista: './vistas/login.html', modelo: './vistas/login' }] = "vistaLogin";
        EnumVistas[EnumVistas["vistaDashboard"] = { vista: './vistas/dashboard.html', modelo: './vistas/dashboard' }] = "vistaDashboard";
        EnumVistas[EnumVistas["vistaWidgets"] = { vista: './widgets.html', modelo: './widgets' }] = "vistaWidgets";
        EnumVistas[EnumVistas["vistaAltaSolicitudes"] = { vista: './alta-solicitudes.html', modelo: './alta-solicitudes' }] = "vistaAltaSolicitudes";
        EnumVistas[EnumVistas["vistaHistorialUsuario"] = { vista: './historial-usuario.html', modelo: './historial-usuario' }] = "vistaHistorialUsuario";
    })(EnumVistas = exports.EnumVistas || (exports.EnumVistas = {}));
});



define('enumeradores/enum-respuesta-api',["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var EnumRespuestaAPI;
    (function (EnumRespuestaAPI) {
        EnumRespuestaAPI[EnumRespuestaAPI["Aceptado"] = 200] = "Aceptado";
        EnumRespuestaAPI[EnumRespuestaAPI["NoEncontrado"] = 404] = "NoEncontrado";
        EnumRespuestaAPI[EnumRespuestaAPI["ErrorInterno"] = 503] = "ErrorInterno";
        EnumRespuestaAPI[EnumRespuestaAPI["ValidacionReglaNegocio"] = 1002] = "ValidacionReglaNegocio";
        EnumRespuestaAPI[EnumRespuestaAPI["NoPermitido"] = 409] = "NoPermitido";
    })(EnumRespuestaAPI = exports.EnumRespuestaAPI || (exports.EnumRespuestaAPI = {}));
});



define('enumeradores/enum-estatus',["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var EnumEstatus;
    (function (EnumEstatus) {
        EnumEstatus[EnumEstatus["Pendiente"] = 1] = "Pendiente";
        EnumEstatus[EnumEstatus["Rechazada"] = 2] = "Rechazada";
        EnumEstatus[EnumEstatus["Autorizada"] = 3] = "Autorizada";
    })(EnumEstatus = exports.EnumEstatus || (exports.EnumEstatus = {}));
});



define('dto/dtousuarios',["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var DtoUsuarios = (function () {
        function DtoUsuarios() {
            this.Id = '';
            this.NombreUsuario = '';
            this.Navegador = navigator.userAgent;
        }
        return DtoUsuarios;
    }());
    exports.DtoUsuarios = DtoUsuarios;
});



define('dto/dtopagos',["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var DtoPagos = (function () {
        function DtoPagos() {
        }
        return DtoPagos;
    }());
    exports.DtoPagos = DtoPagos;
});



define('dto/dtoSolicitudes',["require", "exports", "./dtopagos"], function (require, exports, dtopagos_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var DtoSolicitudesPendientes = (function () {
        function DtoSolicitudesPendientes() {
        }
        return DtoSolicitudesPendientes;
    }());
    exports.DtoSolicitudesPendientes = DtoSolicitudesPendientes;
    var DtoSolicitudesHistorial = (function () {
        function DtoSolicitudesHistorial() {
        }
        return DtoSolicitudesHistorial;
    }());
    exports.DtoSolicitudesHistorial = DtoSolicitudesHistorial;
    var DtoSolicitudesActuales = (function () {
        function DtoSolicitudesActuales() {
        }
        return DtoSolicitudesActuales;
    }());
    exports.DtoSolicitudesActuales = DtoSolicitudesActuales;
    var DtoSolicitud = (function () {
        function DtoSolicitud() {
            this._id = '';
            this.Descripcion = '';
            this.NombreUsuario = '';
            this.Pagos = new dtopagos_1.DtoPagos();
        }
        return DtoSolicitud;
    }());
    exports.DtoSolicitud = DtoSolicitud;
});



var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('componentes/widgets/perfil',["require", "exports", "aurelia-framework", "modelos/Solicitudes", "../../modelos/seguridad/sesion"], function (require, exports, aurelia_framework_1, Solicitudes_1, sesion_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Perfil = (function () {
        function Perfil(Solicitudes, Sesion) {
            this.Solicitudes = Solicitudes;
            this.Sesion = Sesion;
            Solicitudes.ConsultarSolicitudesPendientes();
        }
        Perfil = __decorate([
            aurelia_framework_1.autoinject,
            __metadata("design:paramtypes", [Solicitudes_1.Solicitudes, sesion_1.Sesion])
        ], Perfil);
        return Perfil;
    }());
    exports.Perfil = Perfil;
});



define('text!componentes/widgets/perfil.html', ['module'], function(module) { module.exports = "<template><div class=\"ui top attached label\"><h4>Información del Usuario</h4></div><h4 class=\"ui header\"><div class=\"content\"> ${Sesion.NombreUsuario} <div class=\"sub header\">Solicitudes Pendientes</div></div></h4><table class=\"ui selectable celled structured table\"><thead><tr><th class=\"rigth aligned ten wide\" rowspan=\"2\">Solicitudes</th><th class=\"center aligned\" rowspan=\"2\">Tiempo Aprox.</th><th class=\"center aligned\" colspan=\"2\">Votaciones</th></tr><tr><th class=\"center aligned\"><i class=\"large red thumbs down outline icon\"></i></th><th class=\"center aligned\"><i class=\"large green thumbs up outline icon\"></i></th></tr></thead><tbody repeat.for=\"item of Solicitudes.SolicitudesPendientes\"><tr style=\"display:${item.Visto}\"><td><h4 class=\"ui header\"><div class=\"content\"> ${item.Descripcion} <div class=\"sub header\">${item.Pagos} pagos: ${item.PorcentajeInteres} % de interés.</div><div class=\"sub header\">Monto: ${item.Monto}</div></div></h4></td><td class=\"center aligned\">${item.TiempoRestante}</td><td class=\"center aligned\">${item.vtnRechazados}</td><td class=\"center aligned\">${item.vtnAprobados}</td></tr></tbody></table></template>"; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('componentes/widgets/listado-solicitudes',["require", "exports", "aurelia-framework", "aurelia-event-aggregator", "eventos/eventos", "enumeradores/enum-vistas", "modelos/Solicitudes"], function (require, exports, aurelia_framework_1, aurelia_event_aggregator_1, eventos, enum_vistas_1, Solicitudes_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ListadoSolicitudes = (function () {
        function ListadoSolicitudes(Solicitudes, ea) {
            this.Solicitudes = Solicitudes;
            this.ea = ea;
            Solicitudes.ConsultarSolicitudesActuales();
        }
        ListadoSolicitudes.prototype.HistorialUsuario = function (item) {
            this.Solicitudes.NombreUsuarioHistorial = item.NombreUsuario;
            this.ea.publish(new eventos.AreaTrabajo(enum_vistas_1.EnumVistas.vistaHistorialUsuario));
        };
        ListadoSolicitudes = __decorate([
            aurelia_framework_1.autoinject,
            __metadata("design:paramtypes", [Solicitudes_1.Solicitudes, aurelia_event_aggregator_1.EventAggregator])
        ], ListadoSolicitudes);
        return ListadoSolicitudes;
    }());
    exports.ListadoSolicitudes = ListadoSolicitudes;
});



define('text!componentes/widgets/listado-solicitudes.html', ['module'], function(module) { module.exports = "<template><div class=\"ui top attached label\"><h4>Solicitudes Actuales</h4></div><table class=\"ui selectable celled table\"><thead><tr><th class=\"rigth aligned ten wide\">Solicitudes</th><th class=\"center aligned\">Tiempo Aprox.</th><th class=\"center aligned\">Rechazar / Aprobar</th></tr></thead><tbody repeat.for=\"item of Solicitudes.SolicitudesActuales\"><tr style=\"display:${item.Visto}\"><td><h4 class=\"ui header\"><div class=\"content\"><a href=\"#\" click.delegate=\"HistorialUsuario(item)\">${item.NombreUsuario}</a><div class=\"sub header\">${item.Descripcion}</div><div class=\"sub header\">${item.Pagos} pagos: ${item.PorcentajeInteres} % de interés.</div><div class=\"sub header\">Monto: ${item.Monto}</div><div class=\"sub header\">Monto: ${item.Monto}</div></div></h4></td><td class=\"center aligned\">${item.TiempoRestante}</td><td class=\"center aligned\"><div class=\"ui buttons\"><button click.delegate=\"Solicitudes.VotacionRecApr(item, false)\" class=\"ui negative button active\"><i class=\"large black thumbs down outline icon\"></i></button><div class=\"or\"></div><button click.delegate=\"Solicitudes.VotacionRecApr(item, true)\" class=\"ui positive button active\"><i class=\"large black thumbs up outline icon\"></i></button></div></td></tr></tbody></table></template>"; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('componentes/widgets/historial',["require", "exports", "aurelia-framework", "aurelia-event-aggregator", "modelos/Solicitudes", "eventos/eventos", "enumeradores/enum-vistas"], function (require, exports, aurelia_framework_1, aurelia_event_aggregator_1, Solicitudes_1, eventos, enum_vistas_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Historial = (function () {
        function Historial(ea, Solicitudes) {
            this.ea = ea;
            this.Solicitudes = Solicitudes;
            Solicitudes.ConsultarSolicitudesHistorial();
        }
        Historial.prototype.NuevaSolicitud = function () {
            this.ea.publish(new eventos.AreaTrabajo(enum_vistas_1.EnumVistas.vistaAltaSolicitudes));
        };
        Historial = __decorate([
            aurelia_framework_1.autoinject,
            __metadata("design:paramtypes", [aurelia_event_aggregator_1.EventAggregator, Solicitudes_1.Solicitudes])
        ], Historial);
        return Historial;
    }());
    exports.Historial = Historial;
});



define('text!componentes/widgets/historial.html', ['module'], function(module) { module.exports = "<template><div class=\"ui top attached label\"><h4>Historial Crediticio</h4></div><table class=\"ui selectable celled table\"><thead><tr><th class=\"rigth aligned ten wide\">Solicitudes</th><th class=\"center aligned\">Estatus</th></tr></thead><tbody repeat.for=\"item of Solicitudes.SolicitudesHistorial\"><tr class=\"${item.Estatus ? 'positive' : 'negative'}\"><td><h4 class=\"ui header\"><div class=\"content\"> ${item.Descripcion} <div class=\"sub header\">${item.Pagos} pagos: ${item.PorcentajeInteres} de interés.</div><div class=\"sub header\">Monto: ${item.Monto}</div></div></h4></td><td class=\"center aligned\"><i class=\"large thumbs ${item.Estatus ? 'green up' : 'red down'} outline icon\"></i></td></tr></tbody></table><button class=\"ui fluid large teal button\" click.delegate=\"NuevaSolicitud()\">Nueva Solicitud</button></template>"; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('componentes/menu-cabecero',["require", "exports", "aurelia-framework", "aurelia-event-aggregator", "../eventos/eventos", "enumeradores/enum-vistas", "modelos/usuarios", "enumeradores/enum-respuesta-api", "modelos/seguridad/sesion", "../servicios/webapi/api-websocket"], function (require, exports, aurelia_framework_1, aurelia_event_aggregator_1, eventos, enum_vistas_1, usuarios_1, enum_respuesta_api_1, sesion_1, api_websocket_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var MenuCabecero = (function () {
        function MenuCabecero(ea, sesion, Usuarios, apiWebSocket) {
            this.ea = ea;
            this.sesion = sesion;
            this.Usuarios = Usuarios;
            this.apiWebSocket = apiWebSocket;
        }
        MenuCabecero.prototype.WidGets = function () {
            this.ea.publish(new eventos.AreaTrabajo(enum_vistas_1.EnumVistas.vistaWidgets));
        };
        MenuCabecero.prototype.AgregarSolicitud = function () {
            this.ea.publish(new eventos.AreaTrabajo(enum_vistas_1.EnumVistas.vistaAltaSolicitudes));
        };
        MenuCabecero.prototype.CerrarSesion = function () {
            var _this = this;
            this.Usuarios.CerrarSesion(this.sesion.NombreUsuario)
                .then(function (respuesta) {
                if (respuesta.Estatus == enum_respuesta_api_1.EnumRespuestaAPI.Aceptado) {
                    _this.apiWebSocket.cerrar(_this.sesion.NombreUsuario);
                    location.reload(true);
                }
            });
        };
        MenuCabecero = __decorate([
            aurelia_framework_1.autoinject,
            __metadata("design:paramtypes", [aurelia_event_aggregator_1.EventAggregator, sesion_1.Sesion, usuarios_1.Usuarios, api_websocket_1.ApiWebSocket])
        ], MenuCabecero);
        return MenuCabecero;
    }());
    exports.MenuCabecero = MenuCabecero;
});



define('text!componentes/menu-cabecero.html', ['module'], function(module) { module.exports = "<template><div class=\"ui inverted huge borderless fixed fluid menu\"><a class=\"header item\"><h2>Credit-Friends</h2></a><div class=\"right menu\"><a click.delegate=\"WidGets()\" class=\"item\">Widgets</a> <a click.delegate=\"AgregarSolicitud()\" class=\"item\">Agregar Solicitud</a> <a click.delegate=\"CerrarSesion()\" class=\"item\">Cerrar Sesión</a></div></div></template>"; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('app',["require", "exports", "aurelia-framework", "aurelia-event-aggregator", "eventos/eventos", "enumeradores/enum-vistas"], function (require, exports, aurelia_framework_1, aurelia_event_aggregator_1, eventos, enum_vistas_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var App = (function () {
        function App(ea) {
            this.ea = ea;
            this.tiempoMensaje = 4000;
            this.InicializarVariablesParaAlerta();
            this.Subscribe();
            this.AreaTrabajo();
        }
        App.prototype.Subscribe = function () {
            var _this = this;
            this.ea.subscribe(eventos.VistaTrabajo, function (res) {
                _this.Vistas = res.vista.vista;
                _this.VistasModelos = res.vista.modelo;
            });
        };
        App.prototype.AreaTrabajo = function () {
            this.Vistas = enum_vistas_1.EnumVistas.vistaLogin["vista"];
            this.VistasModelos = enum_vistas_1.EnumVistas.vistaLogin["modelo"];
        };
        App.prototype.MandarAlertas = function (claseAlerta, mensaje) {
            var _this = this;
            this.alerta(claseAlerta, mensaje);
            if (this.cerrarMensajeHandle !== undefined) {
                window.clearTimeout(this.cerrarMensajeHandle);
            }
            this.cerrarMensajeHandle = setTimeout(function () {
                _this.cerrarAlerta();
            }, this.tiempoMensaje);
        };
        App.prototype.InicializarVariablesParaAlerta = function () {
            var _this = this;
            this.ea.subscribe(eventos.MostrarMensaje, function (res) {
                _this.MandarAlertas(res.claseAlerta, res.mensaje);
            });
            this.mostrarAlerta = 'hidden';
            this.mensajeAlerta = '';
        };
        App.prototype.alerta = function (clase, mensaje) {
            var _this = this;
            document.getElementById("divAlerta").classList.remove("bounceOut");
            document.getElementById("divAlerta").classList.add("bounceIn");
            setTimeout(function () {
                _this.mensajeAlerta = mensaje;
                _this.claseAlerta = clase;
                _this.mostrarAlerta = '';
            }, 100);
        };
        App.prototype.cerrarAlerta = function () {
            this.mostrarAlerta = 'hidden';
            this.mensajeAlerta = '';
        };
        App = __decorate([
            aurelia_framework_1.autoinject,
            __metadata("design:paramtypes", [aurelia_event_aggregator_1.EventAggregator])
        ], App);
        return App;
    }());
    exports.App = App;
    String.prototype["format"] = function () {
        var base = this;
        for (var ndx = 0; ndx < arguments.length; ndx++) {
            var regexp = new RegExp("\\{" + ndx.toString() + "}", "gi");
            base = base.replace(regexp, arguments[ndx]);
        }
        return base;
    };
});



define('text!app.html', ['module'], function(module) { module.exports = "<template><div id=\"divAlerta\" class=\"ui ${claseAlerta} message animated ${mostrarAlerta}\" style=\"position:absolute;z-index:9999;top:62px;right:0\"><i click.delegate=\"cerrarAlerta()\" class=\"close icon\"></i><div class=\"header\"> ${mensajeAlerta} </div><p></p></div><div><compose view.bind=\"Vistas\" view-model=\"${VistasModelos}\"></compose></div></template>"; });
//# sourceMappingURL=app-bundle.js.map