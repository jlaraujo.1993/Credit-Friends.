import { EnumVistas } from 'enumeradores/enum-vistas';

export class AreaTrabajo{
    constructor(public vista: EnumVistas){}
}
export class VistaTrabajo{
    constructor(public vista: EnumVistas){}
}
export class MostrarMensaje {
    constructor(public claseAlerta: string, public mensaje: string) { }
}
export class ServicioSocket {
    constructor(public _id: any) { }
}

