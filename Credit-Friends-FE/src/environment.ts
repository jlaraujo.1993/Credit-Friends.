export default {
  debug: true,
  testing: true,
  apiUrl: {
    pruebas: 'http://www.mocky.io/v2/5b903ee02e0000ca2aa89e76',
    pruebasPagos: 'http://www.mocky.io/v2/5b9062252e00000c2ba89eb2',
    solicitudes: 'http://localhost:9001/api/solicitudes',
    usuarios: 'http://localhost:9001/api/usuario',
    pagos: 'http://localhost:9001/api/pagos',
    // seguridad: 'http://localhost:9001/api/seguridad',
  },
  socket: {
    servicioSokect: 'ws://localhost:1337/'
  }
};
