import { autoinject } from 'aurelia-framework'; 
import { EventAggregator } from 'aurelia-event-aggregator'; 
import * as eventos from '../eventos/eventos';
import * as DtoSolicitudes from  'dto/dtoSolicitudes'
import { ApiSolicitudes } from 'servicios/webapi/api-solicitudes';
import { EnumRespuestaAPI } from 'enumeradores/enum-respuesta-api';
import { RespuestaApi } from 'servicios/respuesta-api';
import { Sesion } from '../modelos/seguridad/sesion'; 
import { ApiWebSocket } from '../servicios/webapi/api-websocket'; 
import { ApiRespuesta } from "../servicios/respuesta-api";
import { Usuarios } from "../modelos/usuarios";

@autoinject
export class Solicitudes {
    SolicitudesPendientes : DtoSolicitudes.DtoSolicitudesPendientes[] = [];
    SolicitudesHistorial : DtoSolicitudes.DtoSolicitudesHistorial[] = [];
    SolicitudesHistorialUsuario: DtoSolicitudes.DtoSolicitudesHistorial[] = [];
    SolicitudesActuales : DtoSolicitudes.DtoSolicitudesActuales[] = [];
    Solicitud: DtoSolicitudes.DtoSolicitud = new DtoSolicitudes.DtoSolicitud();
    Respuesta: RespuestaApi = new RespuestaApi();
    NombreUsuarioHistorial: string= "";

    constructor(private ea: EventAggregator,private Usuario: Usuarios,private peticion: ApiSolicitudes, private sesion: Sesion, private apiRespuesta: ApiRespuesta, private apiWebSocket: ApiWebSocket){ 
        this.EjecutarTiempo();
        this.SubsCribe();
    }
    SubsCribe(){
         this.ea.subscribe(eventos.ServicioSocket, res => {
            var obj = JSON.parse(res._id);
            if (obj.Peticion == "Votacion"){
                this.ActualizarPendientes(obj);
            } if (obj.Peticion == "AltaSolicitud"){
                this.AgregarSolicitudActual(obj);
            } if (obj.Peticion == "QuitarSolicitudActual"){
                this.QuitarSolicitudActual(obj);
            } 
            if (obj.Peticion == "AgregarSolHistorialActual"){
                this.AgregarSolicitudHistorial(obj);
            }
        });
    } 
    ActualizarPendientes(obj){
        this.SolicitudesPendientes = this.SolicitudesPendientes.map(m => {
            if (m._id == obj.SolicitudId){
                if (obj.Voto)
                    m.vtnAprobados = m.vtnAprobados + 1;
                else
                    m.vtnRechazados = m.vtnRechazados + 1;
            }
            return m
        });
    }

    AgregarSolicitudActual(obj){ 
        var SolActual = new  DtoSolicitudes.DtoSolicitudesActuales();         
        SolActual._id= obj.SolicitudId;
        SolActual.Descripcion= obj.Descripcion;
        SolActual.TiempoRestante= obj.TiempoRestante;
        SolActual.NombreUsuario= obj.NombreUsuario;
        SolActual.Monto = obj.Monto;
        SolActual.Pagos = obj.Pagos;
        SolActual.PorcentajeInteres = obj.PorcentajeInteres;
        setTimeout(() => {
            this.SolicitudesActuales.unshift(SolActual);
        }, 1000); 
    }

    QuitarSolicitudActual(obj){ 
        this.SolicitudesActuales = this.SolicitudesActuales.filter(f => f._id != obj.SolicitudId);
    }

    AgregarSolicitudHistorial(obj: DtoSolicitudes.DtoSolicitudesHistorial){
        this.SolicitudesHistorial.unshift(obj); 
        this.SolicitudesPendientes = this.SolicitudesPendientes.map(s => {
            if (s._id == obj._id)
                s.Visto = 'none';
            return s;
        });
    }

    private EjecutarTiempo(){
        if(this.SolicitudesPendientes.length != 0){
            this.SolicitudesPendientes = this.SolicitudesPendientes.map(s => {   
                s.TiempoRestante = this.CalcularTiempo(s.TiempoRestante);
                if (s.TiempoRestante != '00:00'){
                    if (s.Visto != 'none')
                        s.Visto = '';
                } else { 
                    if (s.Visto != 'none'){
                        s.Visto = 'none';
                        this.ActualizarSolicitud(s._id)
                    }
                } 
                return s; 
            });
        }
        this.SolicitudesActuales = this.SolicitudesActuales.map(s => {
            s.TiempoRestante = this.CalcularTiempo(s.TiempoRestante);
            if (s.TiempoRestante != '00:00'){
                if (s.Visto != 'none')
                    s.Visto = ''; 
            } else { 
                s.Visto = 'none';
                this.ActualizarSolicitud(s._id) 
            } 
            return s;
        });
        setTimeout(() => {
            this.EjecutarTiempo();
        }, 1000);
    }
    ActualizarSolicitud(Id){
        this.peticion.Actualizar(Id)
        .then(result => {
            //this.AgregarSolicitudHistorial(result.Respuesta);

            this.apiRespuesta.ProcesarRespuesta(result);
            if (result.Estatus == EnumRespuestaAPI.Aceptado){ 
                let wsobj = {
                    Peticion: 'QuitarSolicitudActual',
                    SolicitudId: result.Respuesta._id,
                    NombreUsuario: result.Respuesta.NombreUsuario
                };
                this.apiWebSocket.EnviarMensaje(wsobj); 

                let wsobjl = {
                    Peticion: 'AgregarSolHistorialActual',
                    _id: result.Respuesta._id,
                    Descripcion: result.Respuesta.Descripcion,
                    Pagos: result.Respuesta.Pagos,
                    PorcentajeInteres:result.Respuesta.PorcentajeInteres,
                    Monto:result.Respuesta.Monto,
                    Estatus:result.Respuesta.Estatus,
                    NombreUsuario:result.Respuesta.NombreUsuario
                }; 
                this.apiWebSocket.EnviarMensaje(wsobjl);
            } 
        });
    }

    ConsultarSolicitudesPendientes(){
        this.peticion.ConsultarSolicitudesPendientes(this.sesion.NombreUsuario)
        .then(res => {
            this.SolicitudesPendientes = res.Respuesta; 
        });
    }

    ConsultarSolicitudesHistorial(){
        this.peticion.ConsultarSolicitudesHistorial(this.sesion.NombreUsuario)
        .then(res => {
            this.SolicitudesHistorial = res.Respuesta; 
        });
    }
    ConsultarSolicitudesHistorialUsuario(){
        console.log(this.NombreUsuarioHistorial);
        this.peticion.ConsultarSolicitudesHistorial(this.NombreUsuarioHistorial)
        .then(res => {
            console.log(res);
            this.SolicitudesHistorialUsuario = res.Respuesta; 
        });
    }

    ConsultarSolicitudesActuales(){
        this.peticion.ConsultarSolicitudesActuales(this.sesion.NombreUsuario)
        .then(res => {
            this.SolicitudesActuales = res.Respuesta;
        });
    }

    VotacionRecApr(item: DtoSolicitudes.DtoSolicitudesActuales, tipo: boolean){
        let obj = {
            NombreUsuario: this.sesion.NombreUsuario,
            Voto: tipo
        };
        this.peticion.AgregarVotacion(item._id, obj)
        this.SolicitudesActuales = this.SolicitudesActuales.filter(f => f._id != item._id); 
        let wsobj = {
            Peticion: 'Votacion',
            SolicitudId: item._id,
            NombreUsuario: item.NombreUsuario,
            Voto: tipo
        }; 
        this.apiWebSocket.EnviarMensaje(wsobj);
    }

    esEntero(numero) : boolean{
        if (isNaN(numero)){
            return false;
        } else {
            if (numero % 1 == 0) {
                return true;
            } else {
                return true;
            }
        }
    }

    //Guardar Solicitudes
    Guardar() {
        this.Respuesta.Estatus = EnumRespuestaAPI.ValidacionReglaNegocio;
        this.Respuesta.Respuesta = null;
        if (this.Solicitud.Descripcion.trim() == "") {
            this.Respuesta.Mensaje = "Descripción es campo obligatorio.";
            return new Promise<any>(result => { 
                return result(this.Respuesta);    
            }); 
        } else if (this.Solicitud.Monto == undefined) {
            this.Respuesta.Mensaje = "El monto es campo obligatorio.";
            return new Promise<any>(result => { 
                return result(this.Respuesta);    
            }); 
        } else if (!this.esEntero(this.Solicitud.Monto)) {
            this.Respuesta.Mensaje = "El monto debe ser numérico.";
            return new Promise<any>(result => { 
                return result(this.Respuesta);    
            }); 
        } else if (this.Solicitud.Pagos.Pagos == 0) {
            this.Respuesta.Mensaje = "El pago es campo obligatorio.";
            return new Promise<any>(result => { 
                return result(this.Respuesta);    
            }); 
        } else  {
            return new Promise<any>(result => {
                this.Solicitud.NombreUsuario = this.sesion.NombreUsuario;
                this.peticion.Guardar(this.Solicitud)
                .then(res => {
                    return result(res);  
                });                
            }); 
        } 
    }

    //Calcular tiempo restante de las solicitudes
    CalcularTiempo(minutos): string { 
        var spl = minutos.split(":");
        var min = spl[0];
        var seg = String(Number(spl[1]) - 1); 
        
        if (seg == '-1') {
            seg = '59';
            min = '0' + Number(min - 1);
        }

        if (min.length == 1)
            min = '0'+min;
            
        if (seg.length == 1)
            seg = '0'+seg;

        minutos = min+":"+seg;
        return minutos;
    }
}