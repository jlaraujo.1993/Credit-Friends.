import { autoinject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import * as dtopagos from  'dto/dtopagos'
import { ApiPagos } from 'servicios/webapi/api-pagos'

@autoinject
export class Pagos {
    ListadoPagos : dtopagos.DtoPagos[] = []; 
    constructor(private ea: EventAggregator, private peticion: ApiPagos){ 
        
    } 

    //Consultar pagos
    ConsultarPagos(){
        this.peticion.ConsultarPagos()
        .then(res => {
            this.ListadoPagos = res.Respuesta;
        });
    }
}