import { autoinject } from 'aurelia-framework'; 
import { ApiUsuarios } from 'servicios/webapi/api-usuarios'
import { DtoUsuarios } from 'dto/dtousuarios';
import { EnumRespuestaAPI } from 'enumeradores/enum-respuesta-api'; 
import { RespuestaApi } from 'servicios/respuesta-api'; 

@autoinject
export class Usuarios {
    Usuario: DtoUsuarios = new DtoUsuarios();
    Respuesta: RespuestaApi = new RespuestaApi();
    constructor(private peticion: ApiUsuarios){ }

    CerrarSesion(NombreUsuario){
        return new Promise<any>(result => {
            this.peticion.CerrarSesion(NombreUsuario)
            .then(res => { 
                return result(res);
            });             
        });
    }

    Guardar(){ 
        if (this.Usuario.NombreUsuario.trim() == "") {
            this.Respuesta.Estatus = EnumRespuestaAPI.ValidacionReglaNegocio;
            this.Respuesta.Mensaje = "El nombre de usuario es campo obligatorio.";
            this.Respuesta.Respuesta = null; 
            return new Promise<any>(result => { 
                return result(this.Respuesta);    
            }); 
        }else  {
            return new Promise<any>(result => {
                this.peticion.Guardar(this.Usuario)
                .then(res => {
                    return result(res);  
                });                
            }); 
        } 
    }
}