import { autoinject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import * as eventos from 'eventos/eventos';
import { EnumVistas } from 'enumeradores/enum-vistas';


@autoinject
export class App {
    Vistas: string;
    VistasModelos: string;

    claseAlerta: string;
    mostrarAlerta: string;
    mensajeAlerta: string; 
    tiempoMensaje: number = 4000;
    cerrarMensajeHandle: any;

    constructor(private ea: EventAggregator){
        this.InicializarVariablesParaAlerta(); 
        this.Subscribe();
        this.AreaTrabajo();
    }
    
    Subscribe(){
        this.ea.subscribe(eventos.VistaTrabajo, res => {
            this.Vistas = res.vista.vista;
            this.VistasModelos = res.vista.modelo;
        });
    }
    
    AreaTrabajo(){ 
        this.Vistas = EnumVistas.vistaLogin["vista"];
        this.VistasModelos = EnumVistas.vistaLogin["modelo"];
    }

    MandarAlertas(claseAlerta, mensaje) {
        this.alerta(claseAlerta, mensaje);

        if (this.cerrarMensajeHandle !== undefined) {
            window.clearTimeout(this.cerrarMensajeHandle);
        }

        this.cerrarMensajeHandle = setTimeout(() => {
            this.cerrarAlerta();
        }, this.tiempoMensaje);
    }
    
    InicializarVariablesParaAlerta() {
        this.ea.subscribe(eventos.MostrarMensaje, res => {
            this.MandarAlertas(res.claseAlerta, res.mensaje);
        });
        this.mostrarAlerta = 'hidden';
        this.mensajeAlerta = ''; 
    }

    alerta(clase: string, mensaje: string) {
        document.getElementById("divAlerta").classList.remove("bounceOut");
        document.getElementById("divAlerta").classList.add("bounceIn");
        setTimeout(() => {
            this.mensajeAlerta = mensaje;
            this.claseAlerta = clase;
            this.mostrarAlerta = '';
        }, 100);
    }

    cerrarAlerta() {
        this.mostrarAlerta = 'hidden';
        this.mensajeAlerta = '';
    }

}

String.prototype["format"] = function () {
    var base = this;
    for (var ndx = 0; ndx < arguments.length; ndx++) {
        var regexp = new RegExp("\\{" + ndx.toString() + "}", "gi");
        base = base.replace(regexp, arguments[ndx]);
    }
    return base;
}
