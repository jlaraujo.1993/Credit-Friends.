import { autoinject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import * as eventos from 'eventos/eventos';
import { EnumVistas } from 'enumeradores/enum-vistas';
import { Sesion } from 'modelos/seguridad/sesion';
import { Usuarios } from 'modelos/usuarios';
import { EnumRespuestaAPI } from "../enumeradores/enum-respuesta-api";
import { ApiRespuesta } from "../servicios/respuesta-api";
import { ApiWebSocket } from '../servicios/webapi/api-websocket';


@autoinject
export class Login {
    constructor(private ea: EventAggregator, private sesion: Sesion, private apiRespuesta: ApiRespuesta, private Usuarios: Usuarios, private apiWebSocket: ApiWebSocket ){

    }
    Iniciar(){
        this.Usuarios.Guardar()
        .then(respuesta => {
            this.apiRespuesta.ProcesarRespuesta(respuesta);
            if (respuesta.Estatus == EnumRespuestaAPI.Aceptado){
                this.sesion.NombreUsuario = this.Usuarios.Usuario.NombreUsuario;
                this.apiWebSocket.conectar();
                this.ea.publish(new eventos.VistaTrabajo(EnumVistas.vistaDashboard));
                setTimeout(() => {
                    this.ea.publish(new eventos.AreaTrabajo(EnumVistas.vistaWidgets));
                }, 400);
            }
        });
    }
}