import { autoinject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import * as eventos from 'eventos/eventos';
import { EnumVistas } from 'enumeradores/enum-vistas';

@autoinject
export class Dashboard{
    Vista: string;
    VistaModelo: string;
    constructor(private ea: EventAggregator){
        this.Subscribe();
       // this.AreaTrabajo();
    }
    
    Subscribe(){
        this.ea.subscribe(eventos.AreaTrabajo, res => {
            this.Vista = res.vista.vista;
            this.VistaModelo = res.vista.modelo;
        });
    }

    AreaTrabajo(){
        this.Vista = EnumVistas.vistaWidgets["vista"];
        this.VistaModelo = EnumVistas.vistaWidgets["modelo"];
    }    
}