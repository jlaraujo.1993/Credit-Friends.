   
import { autoinject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import {Solicitudes} from 'modelos/Solicitudes';
import * as eventos from 'eventos/eventos';
import { EnumVistas } from 'enumeradores/enum-vistas';

@autoinject
export class HistorialUsuario {
 
    constructor(private ea: EventAggregator,private Solicitudes: Solicitudes){
        //Consultar historial de Solicitudes 
        Solicitudes.ConsultarSolicitudesHistorialUsuario();
    }
    
    Cerrar() {
        this.ea.publish(new eventos.AreaTrabajo(EnumVistas.vistaWidgets));
    } 
}