import { autoinject, observable } from "aurelia-framework";  
import { EventAggregator } from 'aurelia-event-aggregator';
import { Solicitudes } from 'modelos/Solicitudes'
import { Pagos } from 'modelos/pagos';
import { EnumRespuestaAPI } from "../enumeradores/enum-respuesta-api";
import { ApiRespuesta } from "../servicios/respuesta-api";
import *  as eventos from '../eventos/eventos';
import { EnumVistas } from '../enumeradores/enum-vistas';
import { ApiWebSocket } from '../servicios/webapi/api-websocket';

@autoinject
export class AltaSolicitudes {
    @observable PagoId: number;
    Interes: string = '';

    constructor(private ea: EventAggregator, private Solicitudes: Solicitudes, private Pagos: Pagos, private apiRespuesta: ApiRespuesta, private apiWebSocket: ApiWebSocket){
        this.Solicitudes.Solicitud.Descripcion ='';
        this.Solicitudes.Solicitud.Monto = null;
        Pagos.ConsultarPagos();
    }
    Guardar(){
        this.Solicitudes.Guardar()
        .then(respuesta => {
            this.apiRespuesta.ProcesarRespuesta(respuesta);
            if (respuesta.Estatus == EnumRespuestaAPI.Aceptado){
                let wsobj = {
                    Peticion: 'AltaSolicitud',
                    SolicitudId: respuesta.Respuesta._id,
                    Descripcion : respuesta.Respuesta.Descripcion,
                    TiempoRestante : respuesta.Respuesta.TiempoRestante,
                    NombreUsuario: respuesta.Respuesta.NombreUsuario,
                    Pagos: respuesta.Respuesta.Pagos,
                    PorcentajeInteres: respuesta.Respuesta.PorcentajeInteres,
                    Monto: respuesta.Respuesta.Monto,
                }; 
                this.apiWebSocket.EnviarMensaje(wsobj);
               this.ea.publish(new eventos.AreaTrabajo(EnumVistas.vistaWidgets))
            }
        });
    }

    PagoIdChanged() {
        if(this.PagoId == 0){
            this.Interes = '';
            this.Solicitudes.Solicitud.Pagos.Pagos = 0;
            this.Solicitudes.Solicitud.Pagos.PorcentajeInteres = 0;
        } else {
            var Pagos = this.Pagos.ListadoPagos.filter(s => s._id == String(this.PagoId));
            this.Interes = Pagos[0].PorcentajeInteres + '  % de interes.';            
            this.Solicitudes.Solicitud.Pagos.Pagos = Number(Pagos[0].Pagos);
            this.Solicitudes.Solicitud.Pagos.PorcentajeInteres =  Number(Pagos[0].PorcentajeInteres);
        }
    }
}