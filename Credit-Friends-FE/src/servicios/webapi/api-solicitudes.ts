import { autoinject } from "aurelia-framework";
import environment from 'environment';
import { ApiPeticion } from '../api-peticion';
import { DtoSolicitud } from '../../dto/dtoSolicitudes';

class ApiSolicitudesMethods {
    constructor(private apiBase: string) {
        this.apiBase = this.apiBase + "{0}";
    };
    Guardar() {
        return this.apiBase["format"]('');
    };
    ConsultarSolicitudesPendientes(nombreusuario: string) {
        return this.apiBase["format"]('/pendientes/'+nombreusuario);
    }; 
    ConsultarSolicitudesHistorial(nombreusuario: string) {
        return this.apiBase["format"]('/historial/'+nombreusuario);
    }; 
    ConsultarSolicitudesActuales(nombreusuario: string) {
        return this.apiBase["format"]('/actuales/'+nombreusuario);
    };
    AgregarVotacion(solicitudid: string) {
        return this.apiBase["format"]('/agregarvotacion/'+solicitudid);
    };
    Actualizar(solicitudid: string) {
        return this.apiBase["format"]('/actualizar/'+solicitudid);
    };

    
}

@autoinject
export class ApiSolicitudes {
    apis: ApiSolicitudesMethods;

    constructor(private api: ApiPeticion) {
        this.apis = new ApiSolicitudesMethods(environment.apiUrl.solicitudes);
    }
    
    public Actualizar(solicitudid: string){
        return new Promise<any>(result => {
            this.api.put(this.apis.Actualizar(solicitudid),{})
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }

    public AgregarVotacion(solicitudid: string, obj: any){
        return new Promise<any>(result => {
            this.api.put(this.apis.AgregarVotacion(solicitudid),obj)
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }

    public Guardar(Solicitud: DtoSolicitud){
        return new Promise<any>(result => {
            this.api.post(this.apis.Guardar(), Solicitud)
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }

    public ConsultarSolicitudesPendientes(nombreusuario: string){
        return new Promise<any>(result => {
            this.api.get(this.apis.ConsultarSolicitudesPendientes(nombreusuario))
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }

    public ConsultarSolicitudesHistorial(nombreusuario: string){
        return new Promise<any>(result => {
            this.api.get(this.apis.ConsultarSolicitudesHistorial(nombreusuario))
            .then(respuesta => {
                return result(respuesta);
            });
        });
    } 

    public ConsultarSolicitudesActuales(nombreusuario: string){
        return new Promise<any>(result => {
            this.api.get(this.apis.ConsultarSolicitudesActuales(nombreusuario))
            .then(respuesta => {
                return result(respuesta);
            });
        });
    } 
}
