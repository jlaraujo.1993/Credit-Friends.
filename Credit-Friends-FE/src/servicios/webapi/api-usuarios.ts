import { autoinject } from "aurelia-framework";
import environment from 'environment';
import { ApiPeticion } from '../api-peticion';
import { DtoUsuarios } from 'dto/dtousuarios';


class ApiUsuariosMethods {
    constructor(private apiBase: string) {
        this.apiBase = this.apiBase + "{0}";
    };
    Guardar() {
        return this.apiBase["format"]('');
    };
    CerrarSesion(nombreusuario: string) {
        return this.apiBase["format"]('/cerrarsesion/'+nombreusuario);
    };    
}

@autoinject
export class ApiUsuarios {
    apis: ApiUsuariosMethods;

    constructor(private api: ApiPeticion) {
        this.apis = new ApiUsuariosMethods(environment.apiUrl.usuarios);
    }
    
    public CerrarSesion(nombreusuario: string){
        return new Promise<any>(result => {
            this.api.put(this.apis.CerrarSesion(nombreusuario),{})
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }  
    public Guardar(Usuario: DtoUsuarios){
        return new Promise<any>(result => {
            this.api.post(this.apis.Guardar(), Usuario)
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }
//     public ConsultarPagos(){
//         return new Promise<any>(result => {
//             this.api.get(this.apis.ConsultarPagos())
//             .then(respuesta => {
//                 return result(respuesta);
//             });
//         });
//     } 
 }
