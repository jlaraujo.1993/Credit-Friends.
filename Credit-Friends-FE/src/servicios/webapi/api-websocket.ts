import { autoinject } from "aurelia-framework";
import { EventAggregator } from 'aurelia-event-aggregator';
import environment from 'environment';
import { Sesion } from '../../modelos/seguridad/sesion';
import * as eventos from '../../eventos/eventos';

@autoinject
export class ApiWebSocket {
    socket: WebSocket;

    constructor(private ea: EventAggregator, private sesion: Sesion) {

    }

    private onMessage(ev: MessageEvent){ 
        this.ea.publish(new eventos.ServicioSocket(ev.data)); 
    }

    EnviarMensaje(obj: any){
        if (this.socket && this.socket.readyState == this.socket.OPEN) { 
            this.socket.send(JSON.stringify(obj));
          } else if (this.socket && this.socket.readyState == this.socket.CLOSED) {
            this.conectar();
          }
    }

    conectar(){ 
        this.socket = new WebSocket(environment.socket.servicioSokect+this.sesion.NombreUsuario);
        var self = this;
        this.socket.onmessage = (ev: MessageEvent) => {
           this.onMessage(ev);
         };
    }
    cerrar(nombreusuario) {
        if (this.socket && this.socket.readyState == this.socket.OPEN){
            let obj = {NombreUsuario: nombreusuario, Key: ''};
            this.socket.close(1000,nombreusuario); 
        }
    }
}
