import { autoinject } from "aurelia-framework";
import environment from 'environment';
import { ApiPeticion } from '../api-peticion';


class ApiPagosMethods {
    constructor(private apiBase: string) {
        this.apiBase = this.apiBase + "{0}";
    };
    // GuardarSolicitudes() {
    //     return this.apiBase["format"]('');
    // };
    ConsultarPagos() {
        return this.apiBase["format"]('');
    }; 
    // ConsultarPorCategoria(categoria:string) {
    //     return this.apiBase["format"]('/categoria/'+categoria);
    // };

    
}

@autoinject
export class ApiPagos {
    apis: ApiPagosMethods;

    constructor(private api: ApiPeticion) {
        this.apis = new ApiPagosMethods(environment.apiUrl.pagos);
    }
    
    // public CambiarMeGusta(publicacionid: any){
    //     return new Promise<any>(result => {
    //         this.api.put(this.apis.CambiarMeGusta(publicacionid),{})
    //         .then(respuesta => {
    //             return result(respuesta);
    //         });
    //     });
    // }  
    // public Guardar(publicacion: any){
    //     return new Promise<any>(result => {
    //         this.api.post(this.apis.Guardar(), publicacion)
    //         .then(respuesta => {
    //             return result(respuesta);
    //         });
    //     });
    // }
    public ConsultarPagos(){
        return new Promise<any>(result => {
            this.api.get(this.apis.ConsultarPagos())
            .then(respuesta => {
                return result(respuesta);
            });
        });
    } 
}
