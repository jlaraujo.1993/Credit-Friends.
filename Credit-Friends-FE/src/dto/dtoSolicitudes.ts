import { EnumEstatus } from 'enumeradores/enum-estatus'; 
import { DtoPagos } from './dtopagos';
export class DtoSolicitudesPendientes {
    _id: string;
    Descripcion: string;
    Pagos: string;
    PorcentajeInteres: string;
    Monto: number;
    TiempoRestante: string;
    vtnRechazados: number;
    vtnAprobados: number;
    Visto: string;
}

export class DtoSolicitudesHistorial {
    _id: string;
    Descripcion: string;
    Pagos: string;
    PorcentajeInteres: string;
    Monto: number;
    Estatus: boolean;
    NombreUsuario:string;
}

export class DtoSolicitudesActuales {
    _id: string;
    Descripcion: string;
    TiempoRestante: string;
    NombreUsuario: string;
    Pagos: string;
    PorcentajeInteres: string;
    Monto: number;
    Visto: string;
}

export class DtoSolicitud {
    _id: string;
    Descripcion: string;
    Monto: number;
    Pagos: DtoPagos;
    NombreUsuario:string; 
    FechaCreacion: Date;
    Estatus: EnumEstatus;
    EsActivo: boolean;
    constructor() {
        this._id = '';
        this.Descripcion = '';
        this.NombreUsuario = '';
        this.Pagos = new DtoPagos();
    }
}