import { autoinject } from 'aurelia-framework'; 
import { EventAggregator } from 'aurelia-event-aggregator'; 
import * as eventos from '../eventos/eventos'; 
import { EnumVistas } from 'enumeradores/enum-vistas';  
import { Usuarios } from 'modelos/usuarios'; 
import { EnumRespuestaAPI } from 'enumeradores/enum-respuesta-api'; 
import { Sesion } from 'modelos/seguridad/sesion'; 
import { ApiWebSocket } from '../servicios/webapi/api-websocket';

@autoinject
export class MenuCabecero {
    
    constructor(private ea: EventAggregator, private sesion: Sesion, private Usuarios: Usuarios, private apiWebSocket: ApiWebSocket ){
    
    }
    //Mostrar pantalla de Widgets
    WidGets(){
        this.ea.publish(new eventos.AreaTrabajo(EnumVistas.vistaWidgets));
    }

    //Mostrar pantalla de Nueva Solicitud
    AgregarSolicitud(){
        this.ea.publish(new eventos.AreaTrabajo(EnumVistas.vistaAltaSolicitudes));
    }

    //Cerrar sesion
    CerrarSesion(){
        this.Usuarios.CerrarSesion(this.sesion.NombreUsuario)
        .then(respuesta => {
            if (respuesta.Estatus == EnumRespuestaAPI.Aceptado){
                this.apiWebSocket.cerrar(this.sesion.NombreUsuario);               
                location.reload(true);
            }
        });
    } 
}