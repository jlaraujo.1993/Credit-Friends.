import { autoinject } from "aurelia-framework"; 
import {Solicitudes} from 'modelos/Solicitudes';
import { Sesion } from '../../modelos/seguridad/sesion';

@autoinject 
export class Perfil {
    constructor(private Solicitudes: Solicitudes, private Sesion: Sesion){
        //Consultar Solicitudes pendientes
        Solicitudes.ConsultarSolicitudesPendientes(); 
    }
}