import { autoinject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import {Solicitudes} from 'modelos/Solicitudes';
import * as eventos from 'eventos/eventos';
import { EnumVistas } from 'enumeradores/enum-vistas';

@autoinject
export class Historial {
    constructor(private ea: EventAggregator,private Solicitudes: Solicitudes){
        //Consultar historial de Solicitudes 
        Solicitudes.ConsultarSolicitudesHistorial();
    }
    
    //Nueva Solicitud (Cambiar de vista)
    NuevaSolicitud() {
        this.ea.publish(new eventos.AreaTrabajo(EnumVistas.vistaAltaSolicitudes));
    }
}