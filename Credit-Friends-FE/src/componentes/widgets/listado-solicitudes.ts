import { autoinject } from "aurelia-framework"; 
import { EventAggregator } from 'aurelia-event-aggregator';
import * as eventos from 'eventos/eventos';
import { EnumVistas } from 'enumeradores/enum-vistas';
import {Solicitudes} from 'modelos/Solicitudes' 

@autoinject 
export class ListadoSolicitudes {
    constructor(private Solicitudes: Solicitudes,private ea: EventAggregator){
        //Consultar Solicitudes Actuales
        Solicitudes.ConsultarSolicitudesActuales();
    }
    HistorialUsuario(item){ 
        this.Solicitudes.NombreUsuarioHistorial= item.NombreUsuario;
        this.ea.publish(new eventos.AreaTrabajo(EnumVistas.vistaHistorialUsuario));
    }
}