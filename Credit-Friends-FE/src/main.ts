import {Aurelia} from 'aurelia-framework'
import environment from 'environment';
import {Sesion} from  'modelos/seguridad/sesion';


export function configure(aurelia: Aurelia) {
  aurelia.use
    .standardConfiguration()
    .feature('resources')
    .plugin("aurelia-validation")
    .plugin("moment");

  aurelia.use
        .singleton(Sesion);
        
  if (environment.debug) {
    aurelia.use.developmentLogging();
  }

  if (environment.testing) {
    aurelia.use.plugin('aurelia-testing');
  }

  aurelia.start().then(() => aurelia.setRoot());
}
