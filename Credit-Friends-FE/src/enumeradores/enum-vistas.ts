export enum EnumVistas { 
    vistaNinguna = <any>{vista: '', modelo: ''},
    vistaLogin = <any>{vista: './vistas/login.html', modelo: './vistas/login'},
    vistaDashboard = <any>{vista: './vistas/dashboard.html', modelo: './vistas/dashboard'},
    vistaWidgets = <any>{vista: './widgets.html', modelo: './widgets'},
    vistaAltaSolicitudes = <any>{vista: './alta-solicitudes.html', modelo: './alta-solicitudes'},
    vistaHistorialUsuario = <any>{vista: './historial-usuario.html', modelo: './historial-usuario'}
} 
